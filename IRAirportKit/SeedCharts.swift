//
//  SeedDataOIII.swift
//  IRAirport
//
//  Created by Akbarzade on 5/9/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

//class SeedCharts : SeedProtocol {
//	private var chartHelper : ChartHelper!
//	
//	//Utilize Singleton pattern by instanciating Replicator only once.
//	class var sharedInstance: SeedCharts {
//		struct Singleton {
//			static let instance = SeedCharts()
//		}
//		
//		return Singleton.instance
//	}
//	
//	init(){
////		self.chartHelper = ChartHelper.sharedInstance
//	}
//	
//	/**
//	Pull charts data from a given resource, posts a notification to update
//	datasource of a given/listening ViewController/UITableView
//	
//	- Returns: Void
//	*/
//	func fetchData(){
//	 //Read JSON file in seperate thread
//		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
//			// read JSON file, parse JSON data
//			self.processData(self.readFile())
//			
//			// Post notification to update datasource of a given ViewController/UITableView
//			dispatch_async(dispatch_get_main_queue()) {
//				NSNotificationCenter.defaultCenter().postNotificationName("updateChartsTableData", object: nil)
//			}
//		})
//
//	}
//
///**
//Read JSON data from a local file in the Resources dir.
//
//- Returns: AnyObject The contents of the JSON file.
//*/
//func readFile() -> AnyObject {
//	let dataSourceFilename: String = "OIII"
//	let dataSourceFilenameExtension:String = "json"
//	let filemgr = NSFileManager.defaultManager()
//	let currentPath = NSBundle.mainBundle().pathForResource(dataSourceFilename, ofType: dataSourceFilenameExtension)
//	
//	var jsonResult: AnyObject! = nil
//	
//	do{
//		let jsonData = NSData(contentsOfFile: currentPath!)!
//		
//		if filemgr.fileExistsAtPath(currentPath!){
//			jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)
//		}else{
//			print("\(dataSourceFilename).\(dataSourceFilenameExtension)) does not exist, therefore cannot read JSON data.")
//		}
//	}catch let fetchError as NSError{
//		print("read file error: \(fetchError.localizedDescription)")
//	}
//	
//	return jsonResult
//	
//	}
//	
//	/**
//	Process data from a given resource Chart objects and assigning
//	(additional) property values and calling the Chart Helper to persist Charts
//	to the datastore.
//	
//	- Parameter jsonResult: The JSON content to be parsed and stored to Datastore.
//	- Returns: Void
//	*/
//	func processData(jsonResult: AnyObject?) {
//		var retrievedCharts = [Dictionary<String,AnyObject>]()
//		
//		if let chartList = jsonResult{
//			for index in 0..<chartList.count{
//				var chartItem:Dictionary<String,AnyObject> = chartList[index] as! Dictionary<String,AnyObject>
//				
//				
//				// Create additional chart item properties:
//				chartItem[ChartAttributes.chartChecked.rawValue] = false
//				
//				// Generate Chart UUID
//				
//				retrievedCharts.append(chartItem)
//			}
//		}
////		chartHelper.saveChartsList(retrievedCharts)
//		
//	}
//}