//
//  SeedProtocol.swift
//  IRAirport
//
//  Created by Akbarzade on 5/17/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation

//Methods that must be implemented by every class that extends it.
protocol SeedProtocol {
	func fetchData()
	func processData(jsonResult: AnyObject?)
}