//
//  ChartModel.swift
//  IRAirport
//
//  Created by Akbarzade on 5/9/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit
class ChartModel  {
	var chartId: String
	var airportICAO: String
	var chartType: String
	var chartRunway: String
	var chartChecked: NSNumber
	var chartDescription: String
	var chartImage: NSData
	var chartImageName: String
	var chartIndex: String
	var timeStamp: NSDate
	
	var chartImageWidth: NSNumber?
	var chartImageHeight: NSNumber?
	var locationDegreeLeftSide: NSNumber?
	var locationDegreeButtonSide: NSNumber?
	var locationDegreeTopSide: NSNumber?
	var locationDegreeRightSide: NSNumber?
	var chartAirport: NSObject?
	var airport: Airport?
	
	init(chartType: String , chartIndex : String , chartDescription :String , chartImageName : String ,chartRunawy: String, airportICAO: String ){
		
		self.chartId = "\(airportICAO)|\(chartType)|\(chartRunawy)|\(chartIndex)"
		
		self.airportICAO = airportICAO
		self.chartType = chartType
		self.chartRunway = chartRunawy
		self.chartIndex = chartIndex
		self.chartDescription = chartDescription
		self.chartChecked = NSNumber(bool: false)
		
		self.chartImageName = chartImageName
		self.chartImage = UIImagePNGRepresentation(UIImage(named: chartImageName)!)!
		
		self.chartImageWidth = UIImage(named: chartImageName)?.size.width
		self.chartImageHeight = UIImage(named: chartImageName)?.size.height
		
		
		self.timeStamp = NSDate()
	}
}
