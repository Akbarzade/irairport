//
// Created by Akbarzade on 5/9/16.
// Copyright (c) 2016 Akbarzade. All rights reserved.
//

import Foundation

class AirportModel {
	var airportICAO: String
	var airportName: String
	var airportCountry: String
	var airportCity: String
	var airportIATA: String?
	var airportLocationLatitude: NSNumber?
	var airportLocationLongitude: NSNumber?
	var timeStamp: NSDate
	
	init(airportCountry: String , airportCity : String , airportName :String , airportICAO : String ,  airportLocationLongitude : Double , airportLocationLatitude : Double  , airportIATA : String){
		self.airportCountry = airportCountry
		self.airportCity = airportCity
		self.airportName = airportName
		self.airportICAO = airportICAO
		self.airportLocationLongitude = airportLocationLongitude
		self.airportLocationLatitude = airportLocationLatitude
		self.airportIATA = airportIATA
		self.timeStamp = NSDate()
	}
}
