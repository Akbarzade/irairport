//
//  AirportsSeed.swift
//  IRAirport
//
//  Created by Akbarzade on 5/9/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import CoreData


/**
This is a simple method to counting the Airport Objects

- Parameter
- Returns: Count of Airport's Entities
*/
func checkDataStore(entityToCount: String) -> Int{
	let coreDataHelper = CoreDataHelper()
	let request = NSFetchRequest(entityName: entityToCount)
	let entityCount = coreDataHelper.managedObjectContext.countForFetchRequest(request, error: NSErrorPointer.init())
	print("Total \(entityToCount): \(entityCount)")
	return entityCount
}

/**

Checking the CoreData for Airport Entitie's
If DataStore Hasn't any Airport Object then reading the Special JSON File and import's all Airport available.

*/
func SeedAirports(){
	let entity: String  = "Airport"
	if checkDataStore(entity) == 0 {
		let airportsFileURL = NSBundle.mainBundle().URLForResource("Airports", withExtension: "json")
		let airportService = AirportService()
		airportService.insertJSONAirports(airportsFileURL!)
	}
}

/**

Checking the CoreData for Chart Entitie's
If DataStore Hasn't any Chart Object then reading the Special JSON File and import's all Chart available.

*/
func SeedCharts(){
	let entity: String  = "Chart"
	if checkDataStore(entity) == 0 {
		
		let chartsFileURL = NSBundle.mainBundle().URLForResource("OIII", withExtension: "json")
		let chartService = ChartService()
		
		chartService.insertJSONCharts(chartsFileURL!)
	}
}


