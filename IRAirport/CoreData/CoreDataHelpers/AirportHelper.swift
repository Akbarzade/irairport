//
//  AirportHelper.swift
//  IRAirport
//
//  Created by Akbarzade on 5/9/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import CoreData

/**
This is a Class to handle Airport Entities on CoreData

- Function	newAirport:
- Function	insertAirport:
- Function	insertAirports:
*/
class AirportHelper {
	init() {
	}
	
	// #pragma mark - Core Data Helper
	lazy var coreDataStore: CoreDataStore = {
		let coreDataStore = CoreDataStore()
		return coreDataStore
	}()
	
	lazy var coreDataHelper: CoreDataHelper = {
		let coreDataHelper = CoreDataHelper()
		return coreDataHelper
	}()
	// #pragma mark - Demo
	
	var error: NSError? = nil
	
	// CREATE
	/**
	Function newAirport to insert new Airport into the CoreData
	
	-	Parameter	airport: An AirportModel with the Airport Entity properties
	*/
	func newAirport(airport: AirportModel){
		let AirportEntity: Airport = NSEntityDescription.insertNewObjectForEntityForName(
			"Airport", inManagedObjectContext: coreDataHelper.backgroundContext!) as! Airport
		
		//    if !GetAirport(airport.airportICAO) {
		AirportEntity.airportICAO = airport.airportICAO
		AirportEntity.airportCountry = airport.airportCountry
		AirportEntity.airportCity = airport.airportCity
		AirportEntity.airportName = airport.airportName
		AirportEntity.airportIATA = airport.airportIATA
		AirportEntity.airportLocationLongitude = airport.airportLocationLongitude!
		AirportEntity.airportLocationLatitude = airport.airportLocationLatitude!
		AirportEntity.timeStamp = NSDate()
		//    }
		print("New Airport for \(AirportEntity.airportName) added with ICAO:\(AirportEntity.airportICAO)")
	}
	
	func insertAirport(airport: AirportModel){
		newAirport(airport)
		coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
		
	}
	
	func insertAirports(airports: [AirportModel]){
		for airportItem in airports {
			newAirport(airportItem)
		}
		coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
		print("All New Airports are imported successfully.")
	}
	
	/**
	Inserting Method to import Airports List from passed JSON file.
	
	- Parameter sourceAddress: String Address of JSON file.

	- Returns: NoReturn.
	*/
	/*
	func insertJSONAirports(airportsFileURL: NSURL){
	let data = NSData(contentsOfURL: airportsFileURL)!
		do{
			
			let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
			
			let jsonArray = jsonResult.valueForKey("Airport") as! NSArray
			
			for airport in jsonArray{
				let airportEntity = NSEntityDescription.insertNewObjectForEntityForName(
					"Airport", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! Airport
				
				airportEntity.airportCountry = airport["airportCountry"] as! String
				airportEntity.airportICAO = airport["airportICAO"] as! String
				airportEntity.airportName = airport["airportName"] as! String
				airportEntity.airportCity = airport["airportCity"] as! String
				airportEntity.airportIATA = airport["airportIATA"] as? String
				airportEntity.airportLocationLongitude = airport["airportLocationLongitude"] as! NSNumber
				airportEntity.airportLocationLatitude = airport["airportLocationLatitude"] as! NSNumber
				airportEntity.timeStamp = NSDate()
			
			}
			coreDataHelper.saveContext()
			
		} catch {
			fatalError("Error in inserting Airports from JSON file.")
		}
		
		let request = NSFetchRequest(entityName: "Airport")
		let AirportCount = coreDataHelper.managedObjectContext.countForFetchRequest(request, error: NSErrorPointer.init())
		print("Total Airports: \(AirportCount)")
	}
	*/
	//
	// READ
	
	func GetAirportsCount() -> Int{
		let AirportsCountRequest = NSFetchRequest(entityName: "Airport")
		let airportsCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(AirportsCountRequest, error: NSErrorPointer.init())
		return airportsCount
	}
	
	//	func GetAirportInitChartId(ICAO: String) -> String {
	//		var InitChartId: String = "ERROR"
	//
	//		let airport = GetAirport(ICAO)
	//
	//		InitChartId = airport.airportInitChart!
	//		return InitChartId
	//	}
	
	func GetAirportEntity(ICAO: String) -> Airport{
		let GetAirportFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		GetAirportFetchRequest.predicate = NSPredicate(format:"airportICAO == %@", ICAO)
		
		var airportsResult: [AnyObject]?
		do {
			airportsResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetAirportFetchRequest)
		} catch {
		}
		
		var airports = [Airport]()
		for resultAirportItem in airportsResult! {
			let airportItem = resultAirportItem as! Airport
			airports.append(airportItem)
		}
		
		return airports[0] as Airport
	}
	
	func GetAirport(ICAO: String) -> Airport{
		let GetAirportFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		GetAirportFetchRequest.predicate = NSPredicate(format:"airportICAO == %@", ICAO)
		GetAirportFetchRequest.returnsObjectsAsFaults = false
		
		var airportResult: [AnyObject]?
		do {
			airportResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetAirportFetchRequest)
		} catch {
		}
		
		return (airportResult![0] as? Airport)!
	}
	
	func GetAirports()  -> [Airport]{
		let GetAirportsFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		let sorter: NSSortDescriptor = NSSortDescriptor(key: "airportICAO" , ascending: true)
		GetAirportsFetchRequest.sortDescriptors = [sorter]
		GetAirportsFetchRequest.returnsObjectsAsFaults = false
		
		var airportsResult: [AnyObject]?
		do {
			airportsResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetAirportsFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			airportsResult = nil
		}catch{
			
		}
		
		var airports = [Airport]()
		for resultAirportItem in airportsResult! {
			let airportItem = resultAirportItem as! Airport
			print("Airport : \(airportItem.airportICAO): \(airportItem.airportName) returned")
			airports.append(airportItem)
		}
		return airports
	}
	
	// UPDATE
	//  func updateAirport(airport: AirportModel){
	//
	//  }
	
	func updateAirports(airport: AirportModel){
		
	}
	// DELETE
	func DeleteAirport(ICAO: String){
		
	}
	
	func DeleteAllAirports()
	{
		let fetchMyRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		var results: [AnyObject]?
		
		do {
			results = try self.coreDataHelper.backgroundContext!.executeFetchRequest(fetchMyRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			results = nil
		} catch {
		}
		
		for resultItem in results! {
			let airportItem = resultItem as! Airport
			self.coreDataHelper.backgroundContext!.deleteObject(airportItem)
			print("\(airportItem.airportName) with ICAO:\(airportItem.airportICAO) was Deleted!")
		}
		self.coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
	}
	//	{
	//    let fetchMyRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
	//    var result: [AnyObject]?
	//
	//    do {
	//      result = try self.coreDataHelper.backgroundContext!.executeFetchRequest(fetchMyRequest)
	//    } catch let nserror1 as NSError{
	//      error = nserror1
	//      result = nil
	//    } catch {
	//    }
	//
	//    for resultItem in result! {
	//      let airportItem = resultItem as! Airport
	//      self.coreDataHelper.backgroundContext!.deleteObject(airportItem)
	//      print("\(airportItem.airportName) with ICAO:\(airportItem.airportICAO) was Deleted!")
	//    }
	//    self.coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
	//  }
}