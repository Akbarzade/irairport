//
//  ChartHelper.swift
//  IRAirport
//
//  Created by Akbarzade on 5/17/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//


import Foundation
import UIKit
import CoreData

class ChartHelper {
	
//	private var airportICAO = ChartAttributes.airportICAO.rawValue
//	private var chartType = ChartAttributes.chartType.rawValue
//	private var chartRunway = ChartAttributes.chartRunway.rawValue
//	private var chartIndex = ChartAttributes.chartIndex.rawValue
//	private var chartDescription = ChartAttributes.chartDescription.rawValue
//	private var chartChecked = ChartAttributes.chartChecked.rawValue
//	private var chartImageName = ChartAttributes.chartImageName.rawValue
//	private var timeStamp = NSDate()

	
	//Utilize Singleton pattern by instanciating EventAPI only once.
//	class var sharedInstance: ChartHelper {
//		struct Singleton {
//			static let instance = ChartHelper()
//		}
//		
//		return Singleton.instance
//	}

	
	//
	// MARK: - Initiate and Declratation's
	init() {
	}
	
	let airportHelper = AirportHelper()
//	internal var fetchedResultController = NSFetchedResultsController()
	// #pragma mark - Core Data Helper
	lazy var coreDataStore: CoreDataStore = {
		let coreDataStore = CoreDataStore()
		return coreDataStore
	}()
	
	lazy var coreDataHelper: CoreDataHelper = {
		let coreDataHelper = CoreDataHelper()
		return coreDataHelper
	}()
	// #pragma mark - Demo
	var error: NSError? = nil
	
	
	//
	// MARK: - CREATE
//	func saveChart(chartDetails: Dictionary<String, AnyObject>) {
//		//Create new Object of Chart entity
//		let chartItem = NSEntityDescription.insertNewObjectForEntityForName("Chart",		                                                                    inManagedObjectContext: coreDataHelper.backgroundContext!) as! Chart
//		
//		//Assign field values
//		for (key, value) in chartDetails {
//			for attribute in ChartAttributes.getAll {
//				if (key == attribute.rawValue) {
//					chartItem.setValue(value, forKey: key)
//				}
//			}
//		}
//		
//		coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
//	}
//	
//	func saveChartsList(chartsList: Array<AnyObject>){
//		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
//			
//			//Create eventEntity, process member field values
//			for index in 0..<chartsList.count {
//				var chartsItem:Dictionary<String, NSObject> = chartsList[index] as! Dictionary<String, NSObject>
//				
//				//Check that an Event to be stored has a date, title and city.
//				if chartsItem[self.airportICAO] != "" && chartsItem[self.chartType] != ""  && chartsItem[self.chartIndex] != ""  {
//					
//					//Create new Object of Chart entity
//					let ChartEntity = NSEntityDescription.insertNewObjectForEntityForName(
//						"Chart", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! Chart
//					
//					let chartImage = UIImagePNGRepresentation(UIImage(named: chartsItem[self.chartImageName] as! String)!)
//					
//					
//					//Add member field values
//					ChartEntity.setValue(chartsItem[self.airportICAO], forKey: self.airportICAO)
//					ChartEntity.setValue(chartsItem[self.chartType], forKey: self.chartType)
//					ChartEntity.setValue(chartsItem[self.chartIndex], forKey: self.chartIndex)
//					ChartEntity.setValue(chartsItem[self.chartDescription], forKey: self.chartDescription)
//					ChartEntity.setValue(chartsItem[self.chartImageName], forKey: self.chartImageName)
//					ChartEntity.setValue(chartImage, forKey: "chartImage")
//					
//					ChartEntity.setValue(chartsItem[self.chartChecked], forKey: self.chartChecked)
//					
//					ChartEntity.setValue(NSDate(), forKey: self.timeStamp)
//					
//					//Save current work on Minion workers
//					self.coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
//				}
//	}
	
	func newChart(chartModel: ChartModel){
		let ChartEntity = NSEntityDescription.insertNewObjectForEntityForName(
			"Chart", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! Chart
	
		
		let chartId: String = "\(chartModel.airportICAO)|\(chartModel.chartType)|\(chartModel.chartRunway)|\(chartModel.chartIndex)"
		
		ChartEntity.setValue(chartModel.chartId, forKey: "chartId")
		ChartEntity.setValue(chartModel.airportICAO, forKey: "airportICAO")
		ChartEntity.setValue(chartModel.chartType, forKey: "chartType")
		ChartEntity.setValue(chartModel.chartIndex, forKey: "chartIndex")
		ChartEntity.setValue(chartModel.chartDescription, forKey: "chartDescription")
		ChartEntity.setValue(chartModel.chartImageName, forKey: "chartImageName")
		ChartEntity.setValue(chartModel.chartImage, forKey: "chartImage")
		ChartEntity.setValue(chartModel.chartRunway, forKey: "chartRunway")
		ChartEntity.setValue(chartModel.chartChecked, forKey: "chartChecked")
		ChartEntity.setValue(NSDate.init(), forKey: "timeStamp")
		self.coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
		
		print("Chart: \(ChartEntity.chartDescription) for Airport \(ChartEntity.airportICAO) added with Index:\(ChartEntity.chartIndex)")
	}
	
	func insertChart(Chart: ChartModel){
		newChart(Chart)
	}
	
	func insertCharts(Charts: [ChartModel]){
		for ChartItem in Charts {
			newChart(ChartItem)
		}
		print("All New Charts are imported successfully.")
	}
	
	
	/**
	Inserting Method to import Airports List from passed JSON file.
	
	- Parameter sourceAddress: String Address of JSON file.
	
	- Returns: NoReturn.
	*/
	/*
	func insertJSONCharts(chartsFileURL: NSURL){
		let data = NSData(contentsOfURL: chartsFileURL)!
		do{
			
			
			let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
			
			let jsonArray = jsonResult.valueForKey("Chart") as! NSArray
			
			for chart in jsonArray{
				let chartEntity = NSEntityDescription.insertNewObjectForEntityForName(
					"Chart", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! Chart
				
				let airportICAO = chart["airportICAO"] as! String
				let chartType = chart["chartType"] as! String
				let chartRunway = chart["chartRunway"] as! String
				let chartIndex = chart["chartIndex"] as! String
				
				let chartId: String = "\(airportICAO)|\(chartType)|\(chartRunway)|\(chartIndex)"
				let chartImageName: String = "\(airportICAO)\(chartType)\(chartIndex)"
				
				let chartImage = UIImage(named: chartImageName)
				let imageData = UIImagePNGRepresentation(chartImage!)
				
				
				chartEntity.chartId = chartId as! String
				chartEntity.airportICAO = airportICAO as! String
				chartEntity.chartType = chartType as! String
				chartEntity.chartRunway = chartRunway as? String
				chartEntity.chartIndex = chartIndex as? String
				chartEntity.chartDescription = chart["chartDescription"] as?  String
				chartEntity.chartChecked = NSNumber(bool: chart["chartChecked"] as! Bool)
				chartEntity.chartImageName = chartImageName as String
				chartEntity.chartImage = imageData
				chartEntity.timeStamp = NSDate()
				
				// MARK: - Fetching and Adding Airport to Chart From Available Airport on CoreData
				let airportsFetchRequest = NSFetchRequest(entityName: "Airport")
				let airports = (try self.coreDataHelper.backgroundContext!.executeFetchRequest(airportsFetchRequest)) as! [Airport]
				let airport = airports.filter({ (a: Airport) -> Bool in
					return a.airportICAO == airportICAO
				}).first
				
				chartEntity.airport = airport
				
				// MARK: - Fetching and Adding ChartInfo to Chart From JSON
				let chartInfoEntity = NSEntityDescription.insertNewObjectForEntityForName(
					"ChartInfo", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! ChartInfo

				chartInfoEntity.chartImageWidth = (chart["chartInfo"] as! NSDictionary)["chartImageWidth"] as? NSNumber
				chartInfoEntity.chartImageHeight = (chart["chartInfo"] as! NSDictionary)["chartImageHeight"] as? NSNumber
				chartInfoEntity.locationDegreeLeftSide = (chart["chartInfo"] as! NSDictionary)["locationDegreeLeftSide"] as? NSNumber
				chartInfoEntity.locationDegreeRightSide = (chart["chartInfo"] as! NSDictionary)["locationDegreeRightSide"] as? NSNumber
				chartInfoEntity.locationDegreeTopSide = (chart["chartInfo"] as! NSDictionary)["locationDegreeTopSide"] as? NSNumber
				chartInfoEntity.locationDegreeButtonSide = (chart["chartInfo"] as! NSDictionary)["locationDegreeButtonSide"] as? NSNumber

				chartEntity.chartInfo = chartInfoEntity
				
			}
			coreDataHelper.saveContext()
			
		} catch {
			fatalError("Error in inserting Airports from JSON file.")
		}
		
		let chartRequest = NSFetchRequest(entityName: "Chart")
		let chartInfoRequest = NSFetchRequest(entityName: "ChartInfo")
		let ChartCount = coreDataHelper.backgroundContext!.countForFetchRequest(chartRequest, error: NSErrorPointer.init())
		let ChartInfoCount = coreDataHelper.managedObjectContext.countForFetchRequest(chartInfoRequest, error: NSErrorPointer.init())
		print("Total Charts: \(ChartCount) with Total ChartInfos: \(ChartInfoCount)")
	}
	*/
	
	//
	// MARK: - READ
	
		func GetChartsCount() -> Int{
			let ChartCountRequest = NSFetchRequest(entityName: "Chart")
			let chartCount = self.coreDataHelper.backgroundContext!.countForFetchRequest(ChartCountRequest, error: NSErrorPointer.init())
			return chartCount
		}
		
		func GetChartsCount(ICAO: String) -> Int{
			let ChartCountRequest = NSFetchRequest(entityName: "Chart")
			ChartCountRequest.predicate = NSPredicate(format:"airportICAO == %@", ICAO)
			
			let chartCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(ChartCountRequest, error: NSErrorPointer.init())
			return chartCount
		}
	
	func GetChartsCountByType(ICAO: String, ChartType: String) -> Int{
		let ChartCountRequest = NSFetchRequest(entityName: "Chart")
		ChartCountRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@", ICAO, ChartType)
		
		let chartCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(ChartCountRequest, error: NSErrorPointer.init())
		return chartCount
	}
	
	func GetCheckedChartsCountByType(ICAO: String, ChartType: String) -> Int{
		let ChartCountRequest = NSFetchRequest(entityName: "Chart")
		ChartCountRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartChecked == true", ICAO, ChartType)
		
		let chartCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(ChartCountRequest, error: NSErrorPointer.init())
		return chartCount
	}
	
	func GetChartByChartID(ChartID: String) -> Chart{
		
		let GetChartByIDFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartByIDFetchRequest.predicate = NSPredicate(format:"chartId == %@", ChartID)
		GetChartByIDFetchRequest.returnsObjectsAsFaults = false
		
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartByIDFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		return ChartResult![0] as! Chart
	}
	
	func GetTypeChart(ICAO: String , chartType: String ,chartIndex: String) -> Chart{
		
		let GetChartsChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartsChartFetchRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartIndex == %@", ICAO, chartType, chartIndex)
		GetChartsChartFetchRequest.returnsObjectsAsFaults = false
		
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartsChartFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		return ChartResult![0] as! Chart
	}

	func GetCheckedTypeChartsId(ICAO: String ,chartType: ChartType) -> [String] {
		let GetChartsChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		let airportPredicate	= NSPredicate(format:"airportICAO == %@", ICAO)
		let chartTypePredicate = NSPredicate(format:"chartType == %@" , chartType.rawValue)
			let chartCheckedPredicate = NSPredicate(format:"chartChecked == true")
		GetChartsChartFetchRequest.predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [ airportPredicate , chartTypePredicate , chartCheckedPredicate ])
		let sorterRunway: NSSortDescriptor = NSSortDescriptor(key: "chartRunway" , ascending: true)

		let sorterIndex: NSSortDescriptor = NSSortDescriptor(key: "chartIndex" , ascending: true)
		GetChartsChartFetchRequest.sortDescriptors = [sorterRunway, sorterIndex]
		
		GetChartsChartFetchRequest.returnsObjectsAsFaults = false
		
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartsChartFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		
		var CheckedCharts = [String]()
		for ChartResultItem in ChartResult! {
			let ChartItem = ChartResultItem as! Chart
			CheckedCharts.append(ChartItem.chartId)
		}
		return CheckedCharts
	}
	
	func GetCheckedTypeCharts(ICAO: String , chartType: String) -> [Chart]{
		let GetChartsChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartsChartFetchRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartChecked == true", ICAO, chartType)
		let sorter: NSSortDescriptor = NSSortDescriptor(key: "airportICAO" , ascending: true)
		GetChartsChartFetchRequest.sortDescriptors = [sorter]
		GetChartsChartFetchRequest.returnsObjectsAsFaults = false
		
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartsChartFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		
		var charts = [Chart]()
		for ChartResultItem in ChartResult! {
			let ChartItem = ChartResultItem as! Chart
			print("Chart [\(ChartItem.chartId)] Type: \(ChartItem.chartType) Index: \(ChartItem.chartIndex) Description: \(ChartItem.chartDescription) returned")
			charts.append(ChartItem)
		}
		return charts
}
	
	
	func GetTypeCharts(ICAO: String , chartType: String) -> [Chart]{
		let GetChartsChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartsChartFetchRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@", ICAO, chartType)
	
		let sorterRunway: NSSortDescriptor = NSSortDescriptor(key: "chartRunway" , ascending: true)
		let sorterIndex: NSSortDescriptor = NSSortDescriptor(key: "chartIndex" , ascending: true)
		GetChartsChartFetchRequest.sortDescriptors = [sorterRunway, sorterIndex]
		
		
		GetChartsChartFetchRequest.returnsObjectsAsFaults = false
	
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartsChartFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		
		var charts = [Chart]()
		for ChartResultItem in ChartResult! {
			let ChartItem = ChartResultItem as! Chart
			
			print("Chart [\(ChartItem.chartId)] Type: \(ChartItem.chartType) Index: \(ChartItem.chartIndex) Description: \(ChartItem.chartDescription) returned")
			charts.append(ChartItem)
		}
		return charts
	}
	
	func GetCharts(ICAO: String) -> [Chart]{
		let GetChartsFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartsFetchRequest.predicate = NSPredicate(format:"airportICAO == %@", ICAO)
//		let sorter: NSSortDescriptor = NSSortDescriptor(key: "airportICAO" , ascending: true)
//		GetChartsFetchRequest.sortDescriptors = [sorter]
		GetChartsFetchRequest.returnsObjectsAsFaults = false
		
		var ChartsResult: [AnyObject]?
		do {
			ChartsResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartsFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartsResult = nil
		}
		
		var airportCharts = [Chart]()
		for ChartsResultItem in ChartsResult! {
			let approachChartItem = ChartsResultItem as! Chart
			print("All Charts : \(approachChartItem.chartIndex): \(approachChartItem.chartDescription) returned")
			airportCharts.append(approachChartItem)
		}
		return airportCharts
	}
	
	//
	// MARK: - Sections
		/*
		func GetChartsRunways(ICAO: String , chartType: String) -> [String]{
			var Sections = Set<String>()
			
			for ChartResultItem in GetTypeCharts(ICAO, chartType: chartType) {
				Sections.insert(ChartResultItem.chartRunway!)
			}
			
			return Sections.sort()
		}
		*/
		func GetChartsByRunway(ICAO: String , chartType: String , Runway: String) -> [Chart]{
			let GetChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
			GetChartFetchRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartRunway == %@", ICAO,chartType, Runway)
			let sorter: NSSortDescriptor = NSSortDescriptor(key: "chartIndex" , ascending: true)
			GetChartFetchRequest.sortDescriptors = [sorter]
			GetChartFetchRequest.returnsObjectsAsFaults = false
			
			var ChartResult: [AnyObject]?
			do {
				ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartFetchRequest)
			} catch let nserror1 as NSError{
				error = nserror1
				ChartResult = nil
			}
			
			var charts = [Chart]()
			for ChartResultItem in ChartResult! {
				let ChartItem = ChartResultItem as! Chart
				charts.append(ChartItem)
			}
			return charts
		}
		

	/*
	func GetChartsRunways(ICAO: String , chartType: String) -> [String]{
		var Sections = Set<String>()
		
		for ChartResultItem in GetTypeCharts(ICAO, chartType: chartType) {
			Sections.insert(ChartResultItem.chartRunway!)
		}
		
		return Sections.sort()
	}
	
	func GetChartsByRunway(ICAO: String , chartType: String , Runway: String) -> [Chart]{
		let GetChartFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		GetChartFetchRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartRunway == %@", ICAO, chartType , Runway)
		let sorter: NSSortDescriptor = NSSortDescriptor(key: "chartIndex" , ascending: true)
		GetChartFetchRequest.sortDescriptors = [sorter]
		GetChartFetchRequest.returnsObjectsAsFaults = false
		
		var ChartResult: [AnyObject]?
		do {
			ChartResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetChartFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			ChartResult = nil
		}
		
		var charts = [Chart]()
		for ChartResultItem in ChartResult! {
			let ChartItem = ChartResultItem as! Chart
			charts.append(ChartItem)
		}
		return charts
	}
	*/
	/*
	func GetChartsCountByRunway(ICAO: String,chartType: String ,  Runway: String) -> Int{
		let ChartCountRequest = NSFetchRequest(entityName: "Chart")
		ChartCountRequest.predicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@ AND chartRunway == %@", ICAO, chartType ,Runway)
		
		let chartCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(ChartCountRequest, error: NSErrorPointer.init())
		return chartCount
	}
	*/
		/*
	func GetChartsCountBySections(ICAO: String, chartType: String) -> [Int]{
		let Sections: [String] = GetChartsRunways(ICAO , chartType: chartType)
		var SectionsCount:[Int] = [Int]()
		
		for i  in 0..<Sections.count{
			SectionsCount.append(GetChartsCountByRunway(currentViewInfo.currentAirportICAO,chartType: chartType , Runway: Sections[i]))
		}
		return SectionsCount
	}
	*/
	/*
	func GetChartsSections(ICAO: String , chartType: String) -> [String : Int]{
		let Sections: [String] = GetChartsRunways(ICAO, chartType: chartType)
		var SectionsCount = [String : Int]()
		
		for Section in Sections{
			SectionsCount[Section] = GetChartsCountByRunway(currentViewInfo.currentAirportICAO, chartType: chartType, Runway: Section)
		}
		return SectionsCount
	}
*/
	
	//
	// MARK: - UPDATE
	
	func updateChartCheckStatus(atAirport airportICAO: String  , chartType: String, onChartIndex chartIndex: String, setCheckStatusTo checkStatus: Bool){
		
		let ChartEntity: NSManagedObject = GetTypeChart(airportICAO,chartType: chartType , chartIndex: chartIndex)
		ChartEntity.setValue(checkStatus, forKey: "chartChecked")
		
		do {
			try ChartEntity.managedObjectContext?.save()
		} catch let nserror1 as NSError{
			error = nserror1
		}
	}
	
	//
	// MARK: - DELETE
	
	func DeleteChart(ICAO: String , chartType: String){
		
	}
	
	func DeleteCharts(ICAO: String , chartType: String){
	}
	
	func DeleteAllCharts(){
		let fetchMyRequest: NSFetchRequest = NSFetchRequest(entityName: "Chart")
		var results: [AnyObject]?
		
		do {
			results = try self.coreDataHelper.backgroundContext!.executeFetchRequest(fetchMyRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			results = nil
		} catch {
		}
		
		for resultItem in results! {
			let ChartItem = resultItem as! Chart
			self.coreDataHelper.backgroundContext!.deleteObject(ChartItem)
			print("\(ChartItem.chartDescription) with Index:\(ChartItem.chartIndex) on Airport: \(ChartItem.airportICAO) was Deleted!")
		}
		self.coreDataHelper.saveContext(self.coreDataHelper.backgroundContext!)
	}
}

