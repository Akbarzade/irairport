//
//  AirportService.swift
//  IRAirport
//
//  Created by Akbarzade on 5/28/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import CoreData

/**
This is a Class to handle Airport Entities on CoreData

- Function	newAirport:
- Function	insertAirport:
- Function	insertAirports:
*/
class AirportService {
	init() {
	}
	
	// MARK: - Core Data Helper
	lazy var coreDataStore: CoreDataStore = {
		let coreDataStore = CoreDataStore()
		return coreDataStore
	}()
	
	lazy var coreDataHelper: CoreDataHelper = {
		let coreDataHelper = CoreDataHelper()
		return coreDataHelper
	}()
	
	
	var error: NSError? = nil
	
	// CREATE
	/**
	Function newAirport to insert new Airport into the CoreData
	
	-	Parameter	airport: An AirportModel with the Airport Entity properties
	*/
	
	
	/**
	Inserting Method to import Airports List from passed JSON file.
	
	- parameter airportsFileURL: String Address of JSON file.
	
	- returns: NO RETURNS
	*/
	func insertJSONAirports(airportsFileURL: NSURL){
		let data = NSData(contentsOfURL: airportsFileURL)!
		do{
			
			let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
			
			let jsonArray = jsonResult.valueForKey("Airport") as! NSArray
			
			for airport in jsonArray{
				let airportEntity = NSEntityDescription.insertNewObjectForEntityForName(
					"Airport", inManagedObjectContext: self.coreDataHelper.backgroundContext!) as! Airport
				
				
				airportEntity.airportCountry = airport["airportCountry"] as! String
				airportEntity.airportICAO = airport["airportICAO"] as! String
				airportEntity.airportName = airport["airportName"] as! String
				airportEntity.airportCity = airport["airportCity"] as! String
				airportEntity.airportIATA = airport["airportIATA"] as? String
				airportEntity.airportRunways = airport["airportRunways"] as? String
				airportEntity.airportLocationLongitude = airport["airportLocationLongitude"] as! NSNumber
				airportEntity.airportLocationLatitude = airport["airportLocationLatitude"] as! NSNumber
				airportEntity.timeStamp = NSDate()
				
			}
			coreDataHelper.saveContext()
			
		} catch {
			fatalError("Error in inserting Airports from JSON file.")
		}
		
		let request = NSFetchRequest(entityName: "Airport")
		let AirportCount = coreDataHelper.managedObjectContext.countForFetchRequest(request, error: NSErrorPointer.init())
		print("Total Airports: \(AirportCount)")
	}
	
	//
	// READ
	
	func GetAirport(airportICAO: String) -> Airport {
		var airport: Airport
		var airports: [Airport]
		let GetAirportFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		GetAirportFetchRequest.returnsObjectsAsFaults = false
		
		do {
			airports = try coreDataHelper.managedObjectContext.executeFetchRequest(GetAirportFetchRequest) as! [Airport]
			airport = airports.filter({(a: Airport) -> Bool in
				return a.airportICAO == airportICAO
			}).first!
			
		} catch {
			fatalError("Error on getting airport")
		}
		
		return airport
	}
	
	func GetAirports() -> [Airport] {
		let GetAirportsFetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Airport")
		
		let sorterCountry: NSSortDescriptor = NSSortDescriptor(key: "airportCountry" , ascending: true)
		let sorterICAO: NSSortDescriptor = NSSortDescriptor(key: "airportICAO" , ascending: true)
		GetAirportsFetchRequest.sortDescriptors = [sorterCountry , sorterICAO]
		GetAirportsFetchRequest.returnsObjectsAsFaults = false
		
		var airportsResult: [AnyObject]?
		do {
			airportsResult = try self.coreDataHelper.managedObjectContext.executeFetchRequest(GetAirportsFetchRequest)
		} catch let nserror1 as NSError{
			error = nserror1
			airportsResult = nil
		}catch{
			
		}
		
		var airports = [Airport]()
		for resultAirportItem in airportsResult! {
			let airportItem = resultAirportItem as! Airport
			airports.append(airportItem)
		}
		return airports
	}
	
	func AirportsCount() -> Int {
		let AirportsCountRequest = NSFetchRequest(entityName: "Airport")
		let airportsCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(AirportsCountRequest, error: NSErrorPointer.init())
		return airportsCount
	}
	
	func AirportsCount(forCountry CountryName: String) -> Int {
		let CountryAirportsCountRequest = NSFetchRequest(entityName: "Airport")
		let countryPredicate  = NSPredicate(format:"airportCountry == %@", CountryName)
		CountryAirportsCountRequest.predicate = countryPredicate

		
		let countryAirportsCount = self.coreDataHelper.managedObjectContext.countForFetchRequest(CountryAirportsCountRequest, error: NSErrorPointer.init())
		return countryAirportsCount
	}
}