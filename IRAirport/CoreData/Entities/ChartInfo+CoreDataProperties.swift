//
//  ChartInfo+CoreDataProperties.swift
//  IRAirport
//
//  Created by Akbarzade on 5/28/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ChartInfo {

    @NSManaged var chartImageHeight: NSNumber?
    @NSManaged var chartImageWidth: NSNumber?
    @NSManaged var locationDegreeButtonSide: NSNumber?
    @NSManaged var locationDegreeLeftSide: NSNumber?
    @NSManaged var locationDegreeRightSide: NSNumber?
    @NSManaged var locationDegreeTopSide: NSNumber?
    @NSManaged var chartRunway: String?
    @NSManaged var locationBeginPercentageLeftSide: NSNumber?
    @NSManaged var locationBeginPercentageRightSide: NSNumber?
    @NSManaged var locationBeginPercentageButtonSide: NSNumber?
    @NSManaged var locationBeginPercentageTopSide: NSNumber?
    @NSManaged var chart: Chart?

}
