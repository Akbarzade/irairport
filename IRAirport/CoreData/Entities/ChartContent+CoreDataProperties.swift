//
//  ChartContent+CoreDataProperties.swift
//  IRAirport
//
//  Created by Akbarzade on 6/1/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ChartContent {

    @NSManaged var chartImage: NSData
    @NSManaged var chart: Chart?

}
