//
//  Chart+CoreDataProperties.swift
//  IRAirport
//
//  Created by Akbarzade on 5/28/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Chart {
	
	@NSManaged var chartId: String
	@NSManaged var airportICAO: String
	@NSManaged var chartType: String
	@NSManaged var chartChecked: NSNumber
	@NSManaged var chartIndex: String
	@NSManaged var chartDescription: String
	@NSManaged var isLocational: NSNumber
	@NSManaged var timeStamp: NSDate
	@NSManaged var airport: Airport?
	@NSManaged var chartContent: ChartContent?
	@NSManaged var chartInfo: ChartInfo?

}
