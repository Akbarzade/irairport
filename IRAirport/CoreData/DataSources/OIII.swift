//
//  OIII.swift
//  IRAirport
//
//  Created by Akbarzade on 5/3/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit
import Foundation
import CoreData



var myAirports = [
	AirportModel(airportCountry: "Iran", airportCity: "Tehran", airportName: "Mehrabad International Airport", airportICAO: "OIII", airportLocationLongitude: 35.0, airportLocationLatitude: 50.0, airportIATA: "IKA"),
	AirportModel(airportCountry: "Iran", airportCity: "Tehran", airportName: "Imam Khomeini International Airport", airportICAO: "OIIE", airportLocationLongitude: 35.0, airportLocationLatitude: 50.0, airportIATA: "THR"),
	AirportModel(airportCountry: "Iran", airportCity: "Mashhad", airportName: "Mashhad International Airport (Shahid Hashemi Nejad Airport)", airportICAO: "OIMM", airportLocationLongitude: 35.0, airportLocationLatitude: 50.0, airportIATA: "MHD")
]

var OIIICharts = [

	//REF
	ChartModel(chartType: "REF", chartIndex: "10-1P",  chartDescription: "Airport Briefing (GEN, Arr)", chartImageName: "10-1P", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "REF", chartIndex: "10-1P1", chartDescription: "Airport Briefing (Arr Contd, Dep)", chartImageName: "10-1P1", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "REF", chartIndex: "10-1P2", chartDescription: "Airport Briefing (Dep Contd)", chartImageName: "10-1P2", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "REF", chartIndex: "10-1R",  chartDescription: "Radar Mnm Alts(NORTH)", chartImageName: "10-1R", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "REF", chartIndex: "10-1R1", chartDescription: "Radar Mnm Alts(SOUTH)", chartImageName: "10-1R1", chartRunawy: "General", airportICAO: "OIII"),
	
	//STAR
	ChartModel(chartType: "STAR", chartIndex: "10-2", chartDescription: "BOXAM 1P, 1R, 1V & 1Z, DEHNAMAK 1P & 1V", chartImageName: "10-2", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "STAR", chartIndex: "10-2A", chartDescription: "MIVAK 1R & 1Z, NAGMO 1R & 1V", chartImageName: "10-2A", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "STAR", chartIndex: "10-2B", chartDescription: "PAVET & PAXID 1R & 1Z", chartImageName: "10-2B", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "STAR", chartIndex: "10-2C", chartDescription: "RADAL 1P, 1R & 1V, RUDESHUR 1V & 1Z", chartImageName: "10-2C", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "STAR", chartIndex: "10-2D", chartDescription: "SAVEH 1N, 1R & 1Z, VARAMIN 1R & 1Z", chartImageName: "10-2D", chartRunawy: "General", airportICAO: "OIII"),
	
	//APP
	//runway: "General"
	ChartModel(chartType: "APP", chartIndex: "13-1", chartDescription: "Circling VOR DME 3 Rwy 11L/R", chartImageName: "13-1", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "13-4", chartDescription: "Circling VOR DME-1", chartImageName: "13-4", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "13-5", chartDescription: "Circling VOR DME-2", chartImageName: "13-5", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "16-1", chartDescription: "NDB DME (CIRCLING)", chartImageName: "16-1", chartRunawy: "General", airportICAO: "OIII"),
	
	//runway: "RWY 29L"
	ChartModel(chartType: "APP", chartIndex: "11-1", chartDescription: "ILS-1 Rwy 29L", chartImageName: "11-1", chartRunawy: "RWY 29L", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "11-2", chartDescription: "ILS-2 Rwy 29L", chartImageName: "11-2", chartRunawy: "RWY 29L", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "11-3", chartDescription: "ILS-3 Rwy 29L", chartImageName: "11-3", chartRunawy: "RWY 29L", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "11-4", chartDescription: "ILS-4 Rwy 29L", chartImageName: "11-4", chartRunawy: "RWY 29L", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "13-2", chartDescription: "VOR DME 1 Rwy 29L", chartImageName: "13-2", chartRunawy: "RWY 29L", airportICAO: "OIII"),
	ChartModel(chartType: "APP", chartIndex: "13-3", chartDescription: "VOR DME-2 Rwy 29L", chartImageName: "13-3", chartRunawy: "RWY 29L", airportICAO: "OIII"),

	//TAXI
	ChartModel(chartType: "TAXI", chartIndex: "10-9", chartDescription: "Airport", chartImageName: "10-9", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "TAXI", chartIndex: "10-9A", chartDescription: "Parking Coords, Airport Info, Take-Off Mnms", chartImageName: "10-9A", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "TAXI", chartIndex: "10-9B", chartDescription: "PARKING Stands", chartImageName: "10-9B", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "TAXI", chartIndex: "10-9C", chartDescription: "Parking Stands Contd", chartImageName: "10-9C", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "TAXI", chartIndex: "10-9S", chartDescription: "Standard Mnms", chartImageName: "10-9S", chartRunawy: "General", airportICAO: "OIII"),
	
	//SID
	ChartModel(chartType: "SID", chartIndex: "10-3", chartDescription: "DAXIL 1A & 1B, DEHNAMAK 3A, 3B & 2C", chartImageName: "10-3", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "SID", chartIndex: "10-3A", chartDescription: "EGVEL 2A & 2B, NAGMO 1A & 1B", chartImageName: "10-3A", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "SID", chartIndex: "10-3B", chartDescription: "PAROT 3A, 3B & 4C, PAVET 2A, 2B & 3C", chartImageName: "10-3B", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "SID", chartIndex: "10-3C", chartDescription: "PAXID 3A & 3B", chartImageName: "10-3C", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "SID", chartIndex: "10-3D", chartDescription: "RADAL 2A, 2B & 2C, RUDESHUR 2A & 2B", chartImageName: "10-3D", chartRunawy: "General", airportICAO: "OIII"),
	ChartModel(chartType: "SID", chartIndex: "10-3E", chartDescription: "SAVEH 2A, 2B & 3C, VARAMIN 1A & 1B", chartImageName: "10-3E", chartRunawy: "General", airportICAO: "OIII")

]

