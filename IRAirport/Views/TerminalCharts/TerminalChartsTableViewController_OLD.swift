//
//  TerminalChartsTableViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/26/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TerminalChartsTableViewController_OLD: UITableViewController  {
	
	// MARK: - NOTE
	/// NOTE: - Must be declare and handle
	var ApproachSectionObjects: [TerminalChartModel] = []
	var ApproachChartSectionsCount: [Int:Int] = [:]

	// MARK: - Difinitions
//	@IBOutlet var terminalChartsTableView: UITableView!
	
//	@IBOutlet weak var currentChartTypeLabel: UILabel!
	
	/*
	var ChartObjectArray = [
	TerminalChartModel(TerminalChartType: ChartType.REF.rawValue, ChartItems: []),
	TerminalChartModel(TerminalChartType: ChartType.STAR.rawValue, ChartItems: []),
	TerminalChartModel(TerminalChartType: ChartType.APP.rawValue, ChartItems: []),
	TerminalChartModel(TerminalChartType: ChartType.TAXI.rawValue, ChartItems: []),
	TerminalChartModel(TerminalChartType: ChartType.SID.rawValue, ChartItems: [])
	]
	*/
	
	var ChartObjectsArray: [TerminalChartModel] = []
	
	
	lazy var coreDataStore: CoreDataStore = {
		let coreDataStore = CoreDataStore()
		return coreDataStore
	}()
	
	lazy var coreDataHelper: CoreDataHelper = {
		let coreDataHelper = CoreDataHelper()
		return coreDataHelper
	}()
	
	let chartHelper = ChartHelper()
	let chartService = ChartService()
	let interfaceHandler = InterfaceHandler()
	var interfaceManager =  InterfaceManager()
	var fetchedResultsController: NSFetchedResultsController!
	
	
	let chartCellIdentifier = "ChartCell"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		print("TerminalChartsTableViewController viewDidLoad")
		if Defaults[.isAirportSelected]{
			initChartArray()
		}
		//loadCharts()
	}
	
	override func viewWillAppear(animated: Bool) {
		print("TerminalChartsTableViewController viewWillAppear")
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func loadCharts(){
		
		let GetChartsChartFetchRequest = NSFetchRequest(entityName: "Chart")
		
		let sorterRunway: NSSortDescriptor = NSSortDescriptor(key: "chartRunway" , ascending: true)
		let sorterIndex: NSSortDescriptor = NSSortDescriptor(key: "chartIndex" , ascending: true)
		GetChartsChartFetchRequest.sortDescriptors = [sorterRunway, sorterIndex]
		
		//		let predicate: NSPredicate = NSPredicate(format:"airportICAO == %@ AND chartType == %@", interfaceHandler.SelectedAirport , interfaceHandler.SelectedChartType)
		let predicate: NSPredicate = NSPredicate(format:"airportICAO == %@", interfaceHandler.SelectedChartAirport)
		
		GetChartsChartFetchRequest.predicate = predicate
		
		fetchedResultsController = NSFetchedResultsController(fetchRequest: GetChartsChartFetchRequest, managedObjectContext: coreDataHelper.managedObjectContext, sectionNameKeyPath: "chartRunway"  , cacheName: nil)
		
		
		do{
			try fetchedResultsController.performFetch()
		} catch {
			fatalError("Error in fetching Terminal Charts")
		}
	}
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		if interfaceHandler.SelectedTerminalChart == ChartType.APP {
			return interfaceHandler.AirportApproachChartRunways.count
		}
		return 1
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if Defaults[.isAirportSelected]{
			switch interfaceHandler.SelectedTerminalChart.rawValue {
			case ChartType.REF.rawValue :
				return ChartObjectsArray[ChartType.REF.hashValue].ChartItems.count
			case ChartType.STAR.rawValue :
				return ChartObjectsArray[ChartType.STAR.hashValue].ChartItems.count
			case ChartType.APP.rawValue :
				return ApproachChartSectionsCount[section]!
			case ChartType.TAXI.rawValue :
				return ChartObjectsArray[ChartType.TAXI.hashValue].ChartItems.count
			case ChartType.SID.rawValue :
				return ChartObjectsArray[ChartType.SID.hashValue].ChartItems.count
			default:
				return 0
			}
		}
		return 0
	}
	
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCellWithIdentifier(chartCellIdentifier, forIndexPath: indexPath) as! ChartViewCell
		//var chartItem = Chart()
//		let chartItem = ChartObjectsArray[interfaceHandler.DisplayedTerminalChart.hashValue].ChartItems[indexPath.row]
//		print(chartItem.chartId)
//		configureCell(cell, withObject: chartItem)
//		
		
		switch interfaceHandler.SelectedTerminalChart.hashValue {
		case ChartType.REF.hashValue :
			let chartItem = ChartObjectsArray[ChartType.REF.hashValue].ChartItems[indexPath.row] as Chart
			print(chartItem.chartId)
			configureCell(cell, withObject: chartItem)
			return cell
		case ChartType.STAR.hashValue :
			let chartItem = ChartObjectsArray[ChartType.STAR.hashValue].ChartItems[indexPath.row] as Chart
			print(chartItem.chartId)
			configureCell(cell, withObject: chartItem)
			return cell
		case ChartType.APP.hashValue :
			let chartItem = ChartObjectsArray[ChartType.APP.hashValue].ChartItems[indexPath.row] as Chart
			print(chartItem.chartId)
			configureCell(cell, withObject: chartItem)
			return cell
		case ChartType.TAXI.hashValue :
			let chartItem = ChartObjectsArray[ChartType.TAXI.hashValue].ChartItems[indexPath.row] as Chart
			print(chartItem.chartId)
			configureCell(cell, withObject: chartItem)
			return cell
		case ChartType.SID.hashValue :
			let chartItem = ChartObjectsArray[ChartType.SID.hashValue].ChartItems[indexPath.row] as Chart
			print(chartItem.chartId)
			configureCell(cell, withObject: chartItem)
			return cell
		default:
			break
		}
		
		
		return cell
	}
	
	func configureCell(cell: ChartViewCell, withObject object: Chart){
		if let entityDescription = object.valueForKey("chartDescription") as? String{
			cell.chartDescriptionLabel.text = entityDescription
		}
		if let entityIndex = object.valueForKey("chartIndex") as? String{
			cell.chartIndexLabel.text = entityIndex
		}
		
		if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
			cell.chartCheckButton.selected = entityCheckStatus
		}
		
		cell.didTapButtonHandler = {
			if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
				
				switch currentViewInfo.currentChartType {
				case ChartTypes.REF.rawValue:
					self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.REF.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.STAR.rawValue:
					self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.STAR.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.APP.rawValue:
					self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
					
				case ChartTypes.TAXI.rawValue:
					self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.TAXI.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.SID.rawValue:
					self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.SID.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				default:
					break
				}
				
				self.interfaceHandler.chartCheckedUpdate()
				self.interfaceHandler.fillCheckedCharts()
				let chartId: String = object.valueForKey("chartId") as! String
				let checkStatus: Bool =  object.valueForKey("chartChecked") as! Bool
				debugPrint("ID: \(chartId)\(checkStatus)\(entityCheckStatus)")
				//				self.interfaceHandler.handleCurrentSelectedChart(forChartId: chartId , forChartCheckingStatus: checkStatus)
				
				
				
				// self.loadCharts(currentViewInfo.currentAirportICAO)
			}
		}
	}
	/*
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
	return interfaceHandler.SelectedTerminalChart.stringValueForTableTitle()
	/*
	switch interfaceHandler.SelectedTerminalChart {
	case ChartType.REF:
	return "REFERENCE CHARTS"
	case ChartType.STAR:
	return "ARRIVALS \(currentViewInfo.currentAirportChartsCount[ChartTypes.STAR]!)"
	case ChartType.APP:
	return "APPROACH CHARTS"
	case ChartType.TAXI:
	return "TAXI CHARTS"
	case ChartType.SID:
	return "DEPARTURES"
	default:
	break
	}
	*/
	}
	*/
	
	override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
//		var selectedChart = Chart()
//		let selectedTerminalChart = interfaceHandler.SelectedTerminalChart.rawValue
//		selectedChart = ChartObjectsArray[selectedTerminalChart].ChartItems[indexPath.row]
		var selectedChart = Chart()
		
		switch interfaceHandler.SelectedTerminalChart.rawValue {
		case ChartType.REF.rawValue :
			selectedChart = ChartObjectsArray[ChartType.REF.hashValue].ChartItems[indexPath.row] as Chart
		case ChartType.STAR.rawValue :
			selectedChart = ChartObjectsArray[ChartType.STAR.hashValue].ChartItems[indexPath.row] as Chart
		case ChartType.APP.rawValue :
			selectedChart = ChartObjectsArray[ChartType.APP.hashValue].ChartItems[indexPath.row] as Chart
		case ChartType.TAXI.rawValue :
			selectedChart = ChartObjectsArray[ChartType.TAXI.hashValue].ChartItems[indexPath.row] as Chart
		case ChartType.SID.rawValue :
			selectedChart = ChartObjectsArray[ChartType.SID.hashValue].ChartItems[indexPath.row] as Chart
		default:
			break
		}
		
		NSLog("You WILL selected row: \(indexPath.row) in section: \(indexPath.section)")
		return indexPath
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let chartEntity: Chart = fetchedResultsController.objectAtIndexPath(indexPath) as! Chart
		
		interfaceManager.setSelectedChartId(chartEntity.chartId)
	}
	
	
	func initChartArray(){
		
		let ChartTypeREF  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.REF.rawValue)
		let ChartTypeSTAR  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.STAR.rawValue)
		let ChartTypeAPP  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.APP.rawValue)
		let ChartTypeTAXI  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.TAXI.rawValue)
		let ChartTypeSID = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.SID.rawValue)
		
		ChartObjectsArray = [
			TerminalChartModel(TerminalChartType: ChartType.REF.rawValue, ChartItems: ChartTypeREF),
			TerminalChartModel(TerminalChartType: ChartType.STAR.rawValue, ChartItems: ChartTypeSTAR),
			TerminalChartModel(TerminalChartType: ChartType.APP.rawValue, ChartItems: ChartTypeAPP),
			TerminalChartModel(TerminalChartType: ChartType.TAXI.rawValue, ChartItems: ChartTypeTAXI),
			TerminalChartModel(TerminalChartType: ChartType.SID.rawValue, ChartItems: ChartTypeSID)
		]
		
		if Defaults[.isAirportSelected] {
			
			for sectionIndex in 0..<interfaceHandler.AirportApproachChartRunways.count{
				ApproachChartSectionsCount[sectionIndex] = chartService.GetChartRunwayCountByType(forAirport: Defaults[.SelectedAirport], forChartType: ChartType.APP.rawValue, forChartRunway: interfaceHandler.AirportApproachChartRunways[sectionIndex])
			}
		}
		
		for chart in ChartTypeREF {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeSTAR {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for section in interfaceHandler.AirportApproachChartRunways {
			let sectionItem = section
			print("ChartItem: \(sectionItem)")
		}
		for chart in ChartTypeAPP {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeTAXI {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeSID {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		
	}
	
}


/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
// Get the new view controller using segue.destinationViewController.
// Pass the selected object to the new view controller.
}
*/


/*

extension TerminalChartsTableViewController: InterfaceManagerDelegate{
	func interfaceManagerController(interfaceManager: InterfaceManager, didUpdateChartIndex ChartId: String) {
		
		interfaceHandler.SelectedChartId = ChartId
	}
	
}


protocol TerminalChartsDelegate{
	func setSelectedChart(terminalChart: Chart)
}
*/