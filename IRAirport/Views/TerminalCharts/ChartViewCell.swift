//
//  ChartViewCell.swift
//  IRAirport
//
//  Created by Akbarzade on 5/13/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit

typealias ChartViewCellDidTapButtonHandler = () -> Void

class ChartViewCell: UITableViewCell {
	@IBOutlet weak var chartDescriptionLabel: UILabel!
	@IBOutlet weak var chartIndexLabel: UILabel!
	@IBOutlet weak var chartCheckButton: UIButton!
	
	var didTapButtonHandler: ChartViewCellDidTapButtonHandler?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		setupView()
	}
	// MARK: -
	// MARK: View Methods
	private func setupView() {
		let checkedImage = UIImage(named: "ChartButtonChecked")
		let uncheckedImage = UIImage(named: "ChartButtonUncheck")
		
		chartCheckButton.setImage(uncheckedImage, forState: .Normal)
		chartCheckButton.setImage(uncheckedImage, forState: .Disabled)
		chartCheckButton.setImage(checkedImage, forState: .Selected)
		
		chartCheckButton.addTarget(self, action: "didTapButton:", forControlEvents: .TouchUpInside)
		
	}
	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
	// MARK: -
	// MARK: Actions
	func didTapButton(sender: AnyObject) {
		if let handler = didTapButtonHandler {
			handler()
		}
	}
	
}
