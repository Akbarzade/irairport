//
//  MainViewNavigationBarHandler.swift
//  IRAirport
//
//  Created by Akbarzade on 6/1/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController  {
	func SettingsPopOverHandler(sender: AnyObject){
		
		let VC = storyboard?.instantiateViewControllerWithIdentifier("settingsPopOverViewController") as! SettingsPopOverViewController
		
		
		VC.preferredContentSize = CGSize(width: 270, height: 180)
		
		let settingsNavigationController = UINavigationController(rootViewController: VC)
		settingsNavigationController.modalPresentationStyle = .Popover
		
		let settingsPopOver = settingsNavigationController.popoverPresentationController
		settingsPopOver?.delegate = self
		settingsPopOver?.barButtonItem = sender as? UIBarButtonItem
		
		self.presentViewController(settingsNavigationController, animated: true, completion: nil)
		
	}
	
	func LocationPopOverHandler(sender: AnyObject){
		let VC = storyboard?.instantiateViewControllerWithIdentifier("locationPopOverViewController") as! LocationPopOverViewController
		VC.preferredContentSize = CGSize(width: 360, height: 320)
		
		let locationNavigationController = UINavigationController(rootViewController: VC)
		locationNavigationController.modalPresentationStyle = .Popover
		
		let locationPopOver = locationNavigationController.popoverPresentationController
		locationPopOver?.delegate = self
		locationPopOver?.barButtonItem = sender as? UIBarButtonItem
		
		self.presentViewController(locationNavigationController, animated: true, completion: nil)
	}
	
	func InformationPopOverHandler(sender: AnyObject){
		
		let VC = storyboard?.instantiateViewControllerWithIdentifier("informationPopOverTableViewController") as! InformationPopOverTableViewController
		
		
		VC.preferredContentSize = CGSize(width: 320, height: 600)
		
		let informationNavigationController = UINavigationController(rootViewController: VC)
		informationNavigationController.modalPresentationStyle = .Popover
		
		let informationPopOver = informationNavigationController.popoverPresentationController
		informationPopOver?.delegate = self
		informationPopOver?.barButtonItem = sender as? UIBarButtonItem
		
		self.presentViewController(informationNavigationController, animated: true, completion: nil)
		
	}
}