//
//  LocationPopOverViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/4/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit

class LocationPopOverViewController: UIViewController {

	@IBOutlet weak var currentCoordinateLabel: UILabel!
  @IBOutlet weak var locationPopOverLongitudeLabel: UILabel!
  @IBOutlet weak var locationPopOverLatitudeLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
	
	let currentCoordinateLabelText = "Current Coordinate :" +
 coordinateString(Defaults[.CurrentLocationLatitude], longitude: Defaults[.CurrentLocationLongitude])
	
	currentCoordinateLabel.text = currentCoordinateLabelText
	locationPopOverLatitudeLabel.text = "Latitude[N] \(Defaults[.CurrentLocationLatitude])"
    locationPopOverLongitudeLabel.text = "Longitude[E] \(Defaults[.CurrentLocationLongitude])"
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

	
	func coordinateString(latitude:Double, longitude:Double) -> String {
		var latSeconds = Int(latitude * 3600)
		let latDegrees = latSeconds / 3600
		latSeconds = abs(latSeconds % 3600)
		let latMinutes = latSeconds / 60
		latSeconds %= 60
		var longSeconds = Int(longitude * 3600)
		let longDegrees = longSeconds / 3600
		longSeconds = abs(longSeconds % 3600)
		let longMinutes = longSeconds / 60
		longSeconds %= 60
		return String(format:"%d°%d'%d\"%@ %d°%d'%d\"%@",
		              abs(latDegrees),
		              latMinutes,
		              latSeconds,
		              {return latDegrees >= 0 ? "N" : "S"}(),
		              abs(longDegrees),
		              longMinutes,
		              longSeconds,
		              {return longDegrees >= 0 ? "E" : "W"}() )
	}
	
	
	//coordinateString(-22.9133950, -43.2007100)
}
