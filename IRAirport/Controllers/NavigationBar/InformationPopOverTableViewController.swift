//
//  InformationPopOverTableViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 6/1/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit

class InformationPopOverTableViewController: UITableViewController {
	var interfaceHandler = InterfaceHandler()
	let chartService = ChartService()
	
	@IBOutlet weak var AirportLabel: UILabel!
	
	@IBOutlet weak var ChartIndexLabel: UILabel!
	
	@IBOutlet weak var TerminalTypeLabel: UILabel!
	
	@IBOutlet weak var ChartCheckedLabel: UILabel!
	
	@IBOutlet weak var IsInChartLocationLabel: UILabel!
	
	@IBOutlet weak var IsLocationalLabel: UILabel!
	
	
	@IBOutlet weak var LocationTopSideLabel: UILabel!
	
	@IBOutlet weak var LocationButtonSideLabel: UILabel!
	
	@IBOutlet weak var LocationLeftSideLabel: UILabel!
	
	@IBOutlet weak var LocationRightSideLabel: UILabel!
	
	@IBOutlet weak var ChartImageWidthLabel: UILabel!
	
	@IBOutlet weak var ChartImageHeightLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		handleMainChartInformations()
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(InformationPopOverTableViewController.handleMainChartInformations), name: NotificationKey.TerminalChartSelectionNotificationKey, object: nil)
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
	}
	
	func handleMainChartInformations(){
		if let SelectedChartId: String =  Defaults[.SelectedChartId] {
			if let selectedTerminalChart: Chart  = chartService.GetChart(SelectedChartId) {
				AirportLabel.text = selectedTerminalChart.airportICAO
				ChartIndexLabel.text = selectedTerminalChart.chartIndex
				TerminalTypeLabel.text = ValueForStringChartType(selectedTerminalChart.chartType).fullTypeName()
				ChartCheckedLabel.text = Bool.init(selectedTerminalChart.chartChecked) ? "YES" : "NO"
				IsLocationalLabel.text = Bool.init( selectedTerminalChart.isLocational) ? "YES" : "NO"
				
				LocationTopSideLabel.text = selectedTerminalChart.chartInfo?.locationDegreeTopSide?.stringValue
				
				LocationButtonSideLabel.text = selectedTerminalChart.chartInfo?.locationDegreeButtonSide?.stringValue
				
				LocationLeftSideLabel.text = selectedTerminalChart.chartInfo?.locationDegreeLeftSide?.stringValue
				
				LocationRightSideLabel.text = selectedTerminalChart.chartInfo?.locationDegreeRightSide?.stringValue
				
				ChartImageWidthLabel.text = (selectedTerminalChart.chartInfo?.chartImageWidth?.stringValue)! +
					"-F:\(Defaults[.DisplayedChartContentFrameWidth])-B:\(Defaults[.DisplayedChartContentBoundsWidth])"
				
				ChartImageHeightLabel.text = (selectedTerminalChart.chartInfo?.chartImageHeight?.stringValue)! + "-F:\(Defaults[.DisplayedChartContentFrameHeight])-B:\(Defaults[.DisplayedChartContentBoundsHeight])"
				
				
				if interfaceHandler.IsInArea() == true {
					IsInChartLocationLabel.text = "YES"
				} else {
					IsInChartLocationLabel.text = "NO"
				}
				
				
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 3
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		switch section {
		case 0:
			return 3
		case 1:
			return 6
		case 2:
			return 2
		default:
			return 0
		}
		
	}
	
	/*
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
	let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
	
	// Configure the cell...
	
	return cell
	}
	*/
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
	if editingStyle == .Delete {
	// Delete the row from the data source
	tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
	} else if editingStyle == .Insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
