//
//  MainViewGestureRecognizers.swift
//  IRAirport
//
//  Created by Akbarzade on 5/24/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController  {
	@IBAction func handleTerminalChartTapGesture(recognizer: UITapGestureRecognizer){
		if Defaults[.TerminalChartsTogglingStatus] {
			frameStackViewCharts(interfaceHandler.DisplayedTerminalChart)
		}
		
		let tapPoint: CGPoint = recognizer.locationInView(mainChartImageView)
		view.bringSubviewToFront(locationPointerView)
		print("You Tapped on [X:\(tapPoint.x)][Y:\(tapPoint.y)]")
		locationPointerViewCenterSetup(tapPoint)
	}
	@IBAction func handleTerminalChartPinchGesture(recognizer: UITapGestureRecognizer){
		
	
	}
	
	@IBAction func handleTerminalChartTwoFingerSwipeLeftGesture(recognizer: UISwipeGestureRecognizer) {
		NSLog("TerminalChart Swiped with Two Finger to Left")
	}
	
	@IBAction func handleTerminalChartTwoFingerSwipeRightGesture(recognizer: UISwipeGestureRecognizer) {
		NSLog("TerminalChart Swiped with Two Finger to Right")
	}
	//	@IBAction func handlerMainChartImagePan(recognizer: UIPanGestureRecognizer) {
	//		let translation = recognizer.translationInView(self.view)
	//		if let view = recognizer.view {
	//			view.center = CGPoint(x:view.center.x + translation.x,
	//			                      y:view.center.y + translation.y)
	//		}
	//		recognizer.setTranslation(CGPointZero, inView: self.view)
	//
	//		if recognizer.state == UIGestureRecognizerState.Ended {
	//			// 1
	//			let velocity = recognizer.velocityInView(self.view)
	//			let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
	//			let slideMultiplier = magnitude / 200
	//			print("magnitude: \(magnitude), slideMultiplier: \(slideMultiplier)")
	//
	//			// 2
	//			let slideFactor = 0.1 * slideMultiplier     //Increase for more of a slide
	//			// 3
	//			var finalPoint = CGPoint(x:recognizer.view!.center.x + (velocity.x * slideFactor),
	//			                         y:recognizer.view!.center.y + (velocity.y * slideFactor))
	//			// 4
	//			finalPoint.x = min(max(finalPoint.x, 0), self.view.bounds.size.width)
	//			finalPoint.y = min(max(finalPoint.y, 0), self.view.bounds.size.height)
	//
	//			// 5
	//			UIView.animateWithDuration(Double(slideFactor * 2),
	//			                           delay: 0,
	//			                           // 6
	//				options: UIViewAnimationOptions.CurveEaseOut,
	//				animations: {recognizer.view!.center = finalPoint },
	//				completion: nil)
	//		}
	//	}
	
	/*
	@IBAction func handleswipeTwoFingerGestureLeft(recognizer: UISwipeGestureRecognizer) {
	print("handleSwipeTwoFingerGestureLeft")
	//		interfaceHandler.DisplayNext()
	}
	
	@IBAction func handleswipeTwoFingerGestureRight(recognizer: UISwipeGestureRecognizer) {
	print("handleSwipeTwoFingerGestureRight")
	//		interfaceHandler.DisplayPrevious()
	}
	
	
	@IBAction func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
	if gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed {
	
	let translation = gestureRecognizer.translationInView(self.view)
	// note: 'view' is optional and need to be unwrapped
	gestureRecognizer.view!.center = CGPointMake(gestureRecognizer.view!.center.x + translation.x, gestureRecognizer.view!.center.y + translation.y)
	gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.view)
	}
	}
	
	
	
	func handleTap(recognizer: UITapGestureRecognizer)
	{
	//		UIView.animateWithDuration(0.25){
	//			self.circlePointerLabel.center.y = self.leftNavigationBarMINE.center.y
	//		}
	if !currentViewInfo.currentTogglingStatus{
	frameStackViewCharts(currentViewInfo.currentChartType)
	}
	}
	*/
	
	/*
	// MARK: - scroll view delegate
	func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
		return self.mainChartImageView
	}
	*/
	/*
	func handlePinch(recognizer: UIPinchGestureRecognizer)
	{
		guard let view  = recognizer.view else {
			return
		}
		
		view.transform = CGAffineTransformScale(view.transform, recognizer.scale, recognizer.scale)
		recognizer.scale = 1.0
	}
	*/
	
	//	func handleSwipe(recognizer: UISwipeGestureRecognizer){
	//		guard let view = recognizer.view else {
	//			return
	//		}
	//
	//
	//		view.transform = CGAffineTransformTranslate(view.transform, recognizer.direction.tx, recognizer.direction.ty)
	//
	//		}
	
	
}