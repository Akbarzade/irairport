//
//  MainViewInterfaceHandlers.swift
//  IRAirport
//
//  Created by Akbarzade on 5/24/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController {
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		handleMainChartImageViewSize()
	}
	func refreshInterface(chartId: String){
		let chartEntity: Chart = chartHelper.GetChartByChartID(chartId)
		mainChartImageView.image = UIImage(data: chartEntity.chartContent!.chartImage as NSData)
		
	}
	
	
	func handleMainChartImageView(){
		if let SelectedChartId: String =  Defaults[.SelectedChartId] {
			if let selectedTerminalChart: Chart  = chartService.GetChart(SelectedChartId) {
				if selectedTerminalChart.isLocational == true {
					locationPointerView.hidden = false
				} else if selectedTerminalChart.isLocational == false {
					locationPointerView.hidden = true
				}
				
				print("Defaults[.SelectedChartId] \(selectedTerminalChart.chartId)")
				
				Defaults[.DisplayedChartContentWidth] = selectedTerminalChart.chartInfo!.chartImageWidth!.doubleValue
				Defaults[.DisplayedChartContentHeight] = selectedTerminalChart.chartInfo!.chartImageHeight!.doubleValue
				
				let selectedTerminalChartImage =	UIImage(data: (selectedTerminalChart.chartContent?.chartImage)! as NSData)
				
				handleMainChartImageViewSize()
				mainChartImageView.image = selectedTerminalChartImage
				
				Defaults[.DisplayedChartContentFrameWidth] = Double(mainChartImageView.frame.size.width)
				Defaults[.DisplayedChartContentFrameHeight] = Double(mainChartImageView.frame.size.height)
				
				Defaults[.DisplayedChartContentBoundsWidth] = Double(mainChartImageView.bounds.size.width)
				Defaults[.DisplayedChartContentBoundsHeight] = Double(mainChartImageView.frame.size.height)
			
				
				Defaults[.DisplayedChartContentLatitudeTop] = Double(selectedTerminalChart.chartInfo!.locationDegreeTopSide!)
				Defaults[.DisplayedChartContentLatitudeButton] = Double(selectedTerminalChart.chartInfo!.locationDegreeButtonSide!)
				Defaults[.DisplayedChartContentLongitudeLeft] = Double(selectedTerminalChart.chartInfo!.locationDegreeLeftSide!)
				Defaults[.DisplayedChartContentLongitudeRight] = Double(selectedTerminalChart.chartInfo!.locationDegreeRightSide!)
				Defaults[.isDisplayedTerminalChartLocationaled] = Bool.init(selectedTerminalChart.isLocational)
				
			}
		}
	}
	
	func handleMainChartImageViewSize(){
		//		mainChartImageView.frame = CGRectMake( 0,  0, CGFloat(Defaults[.DisplayedChartContentWidth]), CGFloat(Defaults[.DisplayedChartContentHeight]))
		
		if Defaults.hasKey("DisplayedChartContentWidth") && Defaults.hasKey("DisplayedChartContentHeight") {
			let newSize  = resizedImage(mainChartView.bounds.size)
			print("NEW SIZE = Width: \(newSize.width) Height: \(newSize.height)")
			
			Defaults[.DisplayedChartScaledContentWidth] = Double(newSize.width)
			Defaults[.DisplayedChartScaledContentHeight] = Double(newSize.height)
			
			let x = (mainChartView.frame.width - newSize.width) / 2
			let y = (mainChartView.frame.height - newSize.height) / 2
			
			mainChartImageView.frame = CGRectMake(x,y,newSize.width ,newSize.height)
			//			mainChartImageView.center = mainChartView.center
		}
		
		/*
		adjustConstFullSize(mainChartImageView, parentView: mainChartView)
		*/
	}
	
	
	
	func resizedImage(bounds: CGSize) -> CGSize {
		let size = CGSize.init(width: CGFloat(Defaults[.DisplayedChartContentWidth]), height: CGFloat(Defaults[.DisplayedChartContentHeight]))
		
		var horizontalRatio: CGFloat
		let verticalRatio: CGFloat
		var ratio: CGFloat = 1
		
		if UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation) {
			if Defaults[.TerminalChartsTogglingStatus] {
				horizontalRatio = 450  / size.width
				verticalRatio = 900 / size.height
			} else {
				horizontalRatio = 690  / size.width
				verticalRatio = 900 / size.height
			}
			ratio = min(horizontalRatio, verticalRatio)
		} else {
			if Defaults[.TerminalChartsTogglingStatus] {
				horizontalRatio = bounds.width  / size.width
				verticalRatio = bounds.height / size.height
			} else {
				horizontalRatio = bounds.width  / size.width
				verticalRatio = bounds.height / size.height
			}
			ratio = min(horizontalRatio, verticalRatio)
		}
		
		
		
		let newSize: CGSize = CGSizeMake(size.width * ratio, size.height * ratio)
		return newSize
	}
	
	func adjustConstFullSize(adjustedView: UIImageView!, parentView: UIView!) {
		let topConstraint = NSLayoutConstraint(item: adjustedView,
		                                       attribute: .Top,
		                                       relatedBy: .Equal,
		                                       toItem: parentView,
		                                       attribute: .Top,
		                                       multiplier: 1.0,
		                                       constant: 0.0)
		
		let leftConstraint = NSLayoutConstraint(item: adjustedView,
		                                        attribute: .Leading,
		                                        relatedBy: .Equal,
		                                        toItem: parentView,
		                                        attribute: .Leading,
		                                        multiplier: 1.0,
		                                        constant: 0.0)
		
		let rightConstraint = NSLayoutConstraint(item: adjustedView,
		                                         attribute: .Trailing,
		                                         relatedBy: .Equal,
		                                         toItem: parentView,
		                                         attribute: .Trailing,
		                                         multiplier: 1.0,
		                                         constant: 0.0)
		
		
		let bottomConstraint = NSLayoutConstraint(item: adjustedView,
		                                          attribute: .Bottom,
		                                          relatedBy: .Equal,
		                                          toItem: parentView,
		                                          attribute: .Bottom,
		                                          multiplier: 1.0,
		                                          constant: 0.0)
		
		parentView.addConstraints([topConstraint, leftConstraint, rightConstraint, bottomConstraint])
	}
}

