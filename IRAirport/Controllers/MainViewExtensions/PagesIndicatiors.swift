//
//  PagesIndicatiors.swift
//  IRAirport
//
//  Created by Akbarzade on 5/24/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController {
	
	
	func setupPageIndicatiors(){
		let ICAO: String = currentViewInfo.currentAirportICAO
		var chartId: String = ""
		if let SelectedChartId: String = interfaceHandler.SelectedChartId {
			chartId = SelectedChartId
		}
		chartsPageControlREF.enabled = false
		chartsPageControlSTAR.enabled = false
		chartsPageControlAPP.enabled = false
		chartsPageControlTAXI.enabled  = false
		chartsPageControlSID.enabled = false
		
		chartsPageControlREF.pageIndicatorTintColor = UIColor.lightGrayColor()
		chartsPageControlREF.currentPageIndicatorTintColor = UIColor.lightGrayColor()
		
		chartsPageControlSTAR.pageIndicatorTintColor = UIColor.lightGrayColor()
		chartsPageControlSTAR.currentPageIndicatorTintColor = UIColor.lightGrayColor()
		
		chartsPageControlAPP.pageIndicatorTintColor = UIColor.lightGrayColor()
		chartsPageControlAPP.currentPageIndicatorTintColor = UIColor.lightGrayColor()
		
		chartsPageControlTAXI.pageIndicatorTintColor = UIColor.lightGrayColor()
		chartsPageControlTAXI.currentPageIndicatorTintColor = UIColor.lightGrayColor()
		
		chartsPageControlSID.pageIndicatorTintColor = UIColor.lightGrayColor()
		chartsPageControlSID.currentPageIndicatorTintColor = UIColor.lightGrayColor()
		
		chartsPageControlREFLabel.textColor = UIColor.darkGrayColor()
		chartsPageControlSTARLabel.textColor = UIColor.darkGrayColor()
		chartsPageControlAPPLabel.textColor = UIColor.darkGrayColor()
		chartsPageControlTAXILabel.textColor = UIColor.darkGrayColor()
		chartsPageControlSIDLabel.textColor = UIColor.darkGrayColor()
		
		switch currentViewInfo.currentChartType {
		case "REF":
			chartsPageControlREF.enabled = true
			chartsPageControlStackView.arrangedSubviews[0].hidden = true
			chartsPageControlREFLabel.textColor = currentViewInfo.baseBlueColor
			
			if interfaceHandler.CheckedChartsREF.contains(chartId) {
				chartsPageControlREF.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
				chartsPageControlREF.currentPage = interfaceHandler.CheckedChartsREF.indexOf(chartId)!
			}
			//			for i in 0..<interfaceHandler.CheckedChartsREF.count {
			//				if chartId == interfaceHandler.CheckedChartsREF[i]{
			//					chartsPageControlREF.currentPage = i
			//							chartsPageControlREF.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
			//				}
		//			}
		case "STAR":
			chartsPageControlSTAR.enabled = true
			chartsPageControlStackView.arrangedSubviews[1].hidden = true
			chartsPageControlSTARLabel.textColor = currentViewInfo.baseBlueColor
			
			if interfaceHandler.CheckedChartsSTAR.contains(chartId) {
				chartsPageControlSTAR.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
				chartsPageControlSTAR.currentPage = interfaceHandler.CheckedChartsSTAR.indexOf(chartId)!
			}
		case "APP":
			chartsPageControlAPP.enabled = true
			chartsPageControlStackView.arrangedSubviews[2].hidden = true
			chartsPageControlAPPLabel.textColor  = currentViewInfo.baseBlueColor
			
			//			for i in 0..<interfaceHandler.CheckedChartsAPP.count {
			//				if chartId == interfaceHandler.CheckedChartsAPP[i]{
			//					chartsPageControlAPP.currentPage = i
			//					chartsPageControlAPP.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
			//				}
			//			}
			
			if interfaceHandler.CheckedChartsAPP.contains(chartId) {
				chartsPageControlAPP.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
				chartsPageControlAPP.currentPage = interfaceHandler.CheckedChartsAPP.indexOf(chartId)!
			}
			
		case "TAXI":
			chartsPageControlTAXI.enabled = true
			chartsPageControlStackView.arrangedSubviews[3].hidden = true
			chartsPageControlTAXILabel.textColor  = currentViewInfo.baseBlueColor
			
			if interfaceHandler.CheckedChartsTAXI.contains(chartId) {
				chartsPageControlTAXI.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
				chartsPageControlTAXI.currentPage = interfaceHandler.CheckedChartsTAXI.indexOf(chartId)!
			}
		case "SID":
			chartsPageControlSID.enabled = true
			chartsPageControlStackView.arrangedSubviews[4].hidden = true
			chartsPageControlSIDLabel.textColor  = currentViewInfo.baseBlueColor
			
			if interfaceHandler.CheckedChartsSID.contains(chartId) {
				chartsPageControlSID.currentPageIndicatorTintColor = currentViewInfo.baseBlueColor
				chartsPageControlSID.currentPage = interfaceHandler.CheckedChartsSID.indexOf(chartId)!
			}
		default:
			break
		}
		
		for index in 0...4 {
			chartsPageControlStackView.arrangedSubviews[index].hidden = true
		}
		
		let REFPages: Int = chartHelper.GetCheckedChartsCountByType(ICAO, ChartType: ChartTypes.REF.rawValue)
		let STARPages: Int = chartHelper.GetCheckedChartsCountByType(ICAO, ChartType: ChartTypes.STAR.rawValue)
		let APPPages: Int = chartHelper.GetCheckedChartsCountByType(ICAO, ChartType: ChartTypes.APP.rawValue)
		let TAXIPages: Int = chartHelper.GetCheckedChartsCountByType(ICAO, ChartType: ChartTypes.TAXI.rawValue)
		let SIDPages: Int = chartHelper.GetCheckedChartsCountByType(ICAO, ChartType: ChartTypes.SID.rawValue)
		
		
		
		//		let REFPages: Int =  interfaceHandler.CheckedChartsREF.count
		//		let STARPages: Int = interfaceHandler.CheckedChartsSTAR.count
		//		let APPPages: Int = interfaceHandler.CheckedChartsAPP.count
		//		let TAXIPages: Int = interfaceHandler.CheckedChartsTAXI.count
		//		let SIDPages: Int = interfaceHandler.CheckedChartsSID.count
		
		
		if REFPages > 0 {
			chartsPageControlStackView.arrangedSubviews[0].hidden = false
			chartsPageControlREF.numberOfPages = REFPages
		}
		if STARPages > 0 {
			chartsPageControlStackView.arrangedSubviews[1].hidden = false
			chartsPageControlSTAR.numberOfPages = STARPages
		}
		if APPPages > 0 {
			chartsPageControlStackView.arrangedSubviews[2].hidden = false
			chartsPageControlAPP.numberOfPages = APPPages
		}
		if TAXIPages > 0 {
			chartsPageControlStackView.arrangedSubviews[3].hidden = false
			chartsPageControlTAXI.numberOfPages = TAXIPages
		}
		if SIDPages > 0 {
			chartsPageControlStackView.arrangedSubviews[4].hidden = false
			chartsPageControlSID.numberOfPages = SIDPages
		}
		
	}
	
}
