//
//  MainViewLocationHandler.swift
//  IRAirport
//
//  Created by Akbarzade on 5/24/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import CoreLocation


extension MainViewController : CLLocationManagerDelegate {
	func locationManagerSetup(){
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestAlwaysAuthorization()
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
	}
	
	func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		
		let currentLocation : CLLocation = locations[0] as CLLocation
		
		Defaults[.CurrentLocationLatitude] = currentLocation.coordinate.latitude
		Defaults[.CurrentLocationLongitude] = currentLocation.coordinate.longitude
		
		if Defaults[.isDisplayedTerminalChartLocationaled] == true {
			if interfaceHandler.IsInArea() == true {
				locationPointerView.hidden = false
				locationPointerViewCenterSetup(calculateLocationOnChart())
			} else {
				locationPointerView.hidden = true
			}
		}
		
	}
}