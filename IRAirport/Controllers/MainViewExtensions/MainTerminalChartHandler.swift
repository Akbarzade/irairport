//
//  MainTerminalChartHandlerImageView.swift
//  IRAirport
//
//  Created by Akbarzade on 6/1/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit


extension MainViewController{
	
	
	func locationPointerViewSetup(){
		
		locationPointerView = UIView(frame: CGRect(x: 538 , y: 890, width: 20, height: 20))
		locationPointerView.backgroundColor = UIColor.blueColor()
		locationPointerView.layer.borderWidth = 5
		locationPointerView.layer.borderColor = UIColor.whiteColor().CGColor
		locationPointerView.layer.shadowOffset = CGSize(width: 5, height: 5)
		locationPointerView.layer.shadowRadius = 10.0
		locationPointerView.layer.shadowColor = UIColor.darkGrayColor().CGColor
		
		locationPointerView.layer.cornerRadius = locationPointerView.frame.width / 2
	}
	
	func locationPointerViewCenterSetup(tapPoint: CGPoint){
		let pointOnChartX: CGFloat = tapPoint.x
		let pointOnChartY: CGFloat = tapPoint.y
		let pointOnChart: CGPoint = CGPoint(x: pointOnChartX, y: pointOnChartY)
		
		print("EDITED Tapped Point [X:\(pointOnChart.x)][Y:\(pointOnChart.y)]")

		locationPointerView.center = pointOnChart
		
	}
//	Defaults[.DisplayedChartContentLatitudeTop]
//	Defaults[.DisplayedChartContentLatitudeButton]
//	Defaults[.DisplayedChartContentLongitudeLeft]
//	Defaults[.DisplayedChartContentLongitudeRight]
	
//	35.85 - 35.828305 = 0.021695
//	50.989795 - 50.97 = 0.019795
//	
//	0.04 / 100 = 0.0004
//	0.03 / 100 = 0.0003
//	
//	0.021695 / 0.0004 = 54.2375
//	0.019795 / 0.0003 = 65.9833333

	
	func calculateLocationOnChart() -> CGPoint {
		let userLocationPoint: CGPoint!
		let chartLatitudeDelta = Defaults[.DisplayedChartContentLatitudeTop] - Defaults[.DisplayedChartContentLatitudeButton]
		let chartLongitudeDelta = Defaults[.DisplayedChartContentLongitudeRight] - Defaults[.DisplayedChartContentLongitudeLeft]
		let latitudeDelta =  Defaults[.DisplayedChartContentLatitudeTop] - Defaults[.CurrentLocationLatitude]
		let longitudeDelta = Defaults[.CurrentLocationLongitude] - Defaults[.DisplayedChartContentLongitudeLeft]
		
		let latitudeDeltaPercent = latitudeDelta / chartLatitudeDelta
		let longitudeDeltaPercent = longitudeDelta / chartLongitudeDelta
		
		print("Defaults[.CurrentLocationLatitude]:\(Defaults[.CurrentLocationLatitude]) - Defaults[.CurrentLocationLongitude]\(Defaults[.CurrentLocationLongitude])")
		print("latitudeDeltaPercent:\(latitudeDeltaPercent) - longitudeDeltaPercent\(longitudeDeltaPercent)")
		
		let x = CGFloat(Defaults[.DisplayedChartScaledContentWidth] * longitudeDeltaPercent)
		let y = CGFloat(Defaults[.DisplayedChartScaledContentHeight] * latitudeDeltaPercent)
		print("X: \(x) Y: \(y)")
		userLocationPoint = CGPoint(x: x,y: y)
		
		return userLocationPoint
	}
}