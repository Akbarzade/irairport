//
//  ChartsPageHolderViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/15/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit

class ChartsPageHolderViewController: UIViewController {

	@IBOutlet weak var chartImageView: UIImageView!
	
	var chartContent: UIImage!
	var chartFileName: String!
	var chartType: ChartTypes!
	var chartIndexInType: Int!
	var chartIndex: Int!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
	

    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	}
}
