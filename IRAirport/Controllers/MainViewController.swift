
//  MainViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/3/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation



class MainViewController: UIViewController  , UIPopoverPresentationControllerDelegate,  UIGestureRecognizerDelegate , AirportsDelegate  {
//	let defaults = NSUserDefaults.standardUserDefaults()
//	var Defaults = NSUserDefaults(suiteName: "com.akbarzade.IRAlborz")!

	var interfaceHandler = InterfaceHandler()
	let interfaceManager = InterfaceManager()
	let airportService = AirportService()
	let chartService = ChartService()
	
	let chartHelper = ChartHelper()
	
	var locationManager = CLLocationManager()
	let mainStackView = UIStackView()
	let sideStackView = UIStackView()
	
 var mainSetTerminalViewDelegate: MainSetTerminalViewDelegate!
	
	
	//
	//	var CurrentChartIdToDisplay: String? {
	//		didSet {
	//			if let chartId = CurrentChartIdToDisplay {
	//				refreshInterface(chartId)
	//			}
	//		}
	//	}
	
	//@IBOutlet var tableView: UITableView!
	
	
	@IBOutlet weak var chartsPageControlStackView: UIStackView!
	
	@IBOutlet weak var chartsPageControlREFLabel: UILabel!
	@IBOutlet weak var chartsPageControlREF: UIPageControl!
	@IBOutlet weak var chartsPageControlSTARLabel: UILabel!
	@IBOutlet weak var chartsPageControlSTAR: UIPageControl!
	@IBOutlet weak var chartsPageControlAPPLabel: UILabel!
	@IBOutlet weak var chartsPageControlAPP: UIPageControl!
	@IBOutlet weak var chartsPageControlTAXILabel: UILabel!
	@IBOutlet weak var chartsPageControlTAXI: UIPageControl!
	@IBOutlet weak var chartsPageControlSIDLabel: UILabel!
	@IBOutlet weak var chartsPageControlSID: UIPageControl!
	
	
	@IBOutlet weak var chartsContainerView : UIView!
	@IBOutlet weak var mainChartView: UIView!
	@IBOutlet weak var mainChartImageView: UIImageView!
	var locationPointerView : UIView!
	var chartImageViewer: UIImageView!
	//	@IBOutlet var mainChartImagePanGesture: UIPanGestureRecognizer!
	//	@IBOutlet var swipeTwoFingerGestureLeft: UISwipeGestureRecognizer!
	//	@IBOutlet var swipeTwoFingerGestureRight: UISwipeGestureRecognizer!
	
	
	let sideUIView = UIView()
	let mainUIView = UIView()
	
	let ChartsView = UIView()
	let REFsChartsView = UIView()
	let STARsChartsView = UIView()
	let APPsChartsView = UIView()
	let TAXIsChartsView = UIView()
	let SIDsChartsView = UIView()
	let MineView = UIView()
	
	
	
	
 var  referenceCharts: [Chart]!
	var arrivalCharts: [Chart]!
	var approachCharts: [Chart]!
	var approachChartsCountByRunway: [Int]!
	var taxiCharts: [Chart]!
	var departureCharts: [Chart]!
	
	
	
	@IBOutlet weak var frameStackView: UIStackView!
	@IBOutlet weak var mainNavigation: UINavigationItem!
	//	let swipeGesture = UISwipeGestureRecognizer()
	
	override func viewWillAppear(animated: Bool) {
		setupInitLoad()
		print("MainView viewWillAppear")
	}
	
	override func viewDidLoad() {
		locationManagerSetup()
		locationPointerViewSetup()
		mainChartImageView.addSubview(locationPointerView)
		frameStackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height).insetBy(dx: 0, dy: 0)
		frameStackView.arrangedSubviews[1].hidden = true
		
		print("MainView viewDidLoad")
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleMainChartImageView", name: NotificationKey.TerminalChartSelectionNotificationKey, object: nil)
		
		
		let terminalChartTapGesture = UITapGestureRecognizer(target: self, action: #selector(MainViewController.handleTerminalChartTapGesture(_:)))

				let terminalChartPinchGesture = UITapGestureRecognizer(target: self, action: #selector(MainViewController.handleTerminalChartPinchGesture(_:)))
		
		mainChartImageView.addGestureRecognizer(terminalChartPinchGesture)
		mainChartImageView.addGestureRecognizer(terminalChartTapGesture)
		
		
	}
	
	func setupInitLoad(){
		if  Defaults.hasKey("DisplayedChartId") == true && Defaults[.isAirportSelected] == true {
			interfaceHandler.handleCurrentSelectedChart(forChartId: Defaults[.DisplayedChartId])
		}
		
		if Defaults.hasKey("DisplayedAirport") == true {
			setSelectedAirport(Defaults[.DisplayedAirport])
			setAirportButtonText(Defaults[.DisplayedAirport])
			}
		
		
	}
	
	// reset scale when layout changed
	override func viewWillLayoutSubviews() {
		//
		//		// set scroll view min, max and current zoom scale
		//		let currentZoomScale = self.chartsContainerScrollView.bounds.height/self.chartsContainerScrollView.bounds.size.height
		//		self.chartsContainerScrollView.zoomScale = currentZoomScale
		//self.chartsContainerScrollView.minimumZoomScale =  1.0
		//		self.chartsContainerScrollView.maximumZoomScale = 3.0
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
	}
	
	
	
	@IBAction func mainNavigationBarNavigationButton(sender: AnyObject) {
		
	LocationPopOverHandler(sender)
		
	}
	@IBAction func mainNavigationBarSettingsButton(sender: AnyObject) {
		SettingsPopOverHandler(sender)
	}
	
	@IBAction func informationNavigationBarSettingsButton(sender: AnyObject) {
	InformationPopOverHandler(sender)
	}
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if (segue.identifier == "AirportsListPopOverSegue"){
			let airportsTVC = segue.destinationViewController as! AirportsTableViewController
			airportsTVC.airportsDelegate = self
			print("Segue Detected as : \(segue.identifier!)")
		} else if (segue.identifier == "TerminalChartTableViewSegue" ){
			let terminalChartTVC = segue.destinationViewController as! TerminalChartTableViewController
			
			mainSetTerminalViewDelegate = terminalChartTVC
			
			print("MVC: Segue Detected as : \(segue.identifier!)")
		} else {
			print("Segue not detected.")
		}
		
	}
	
	func setSelectedAirport(ICAO: String) {
		print("Displayed Airport is : \(Defaults[.DisplayedAirport])")
		print("Selected Airport is: \(ICAO)")
		print("Selected Airport is: \(Defaults[.SelectedAirport])")
		
		Defaults[.isAirportSelected] = true
		Defaults[.isTerminalChartSelected] = false
		Defaults[.SelectedAirport] = ICAO
		
		interfaceHandler.SelectedAirportEntity = airportService.GetAirport(ICAO)
		Defaults[.DisplayedAirport] = ICAO
		interfaceHandler.handleAirportApproachRunways()
		print("Displayed Airport Changed to : \(Defaults[.DisplayedAirport])")
		print("Selected Airport is: \(ICAO)")
	}
	
	func setAirportButtonText(ICAO: String) {
		let ICAOLabel = ICAO + "▾"
		leftNavigationBarICAO.setTitle(ICAOLabel, forState: UIControlState.Normal)
		
	}
	
	
	
	// MARK: - Navigation
	//
	//leftNavigationBarCirclePointer
	@IBOutlet weak var circlePointerLabel: UILabel!
 
	//leftNavigationBarICAOButton
	@IBOutlet weak var leftNavigationBarICAO: UIButton!
	@IBAction func leftNavigationBarICAOButton(sender: UIButton) {
		//    let TVC = self.storyboard!.instantiateViewControllerWithIdentifier("AirportsTableViewController") as! UITableViewController
		////    let TVC = self.storyboard!.instantiateViewControllerWithIdentifier("AirportsTableViewController") as! AirportTableViewController
		//    TVC.modalPresentationStyle = .Popover
		//
		//    if let AirportPopOverPresentationController = TVC.popoverPresentationController{
		//
		//      let viewForSource = sender as UIView
		//      AirportPopOverPresentationController.sourceView = viewForSource
		//      AirportPopOverPresentationController.sourceRect  = CGRectMake(0, 0, CGFloat(sender.frame.size.width / 3), CGFloat(sender.frame.size.height / 2))
		//      presentViewController(TVC, animated: true, completion: nil)
		//
		////      TVC.preferredContentSize = CGSizeMake(240,800)
		//
		//      AirportPopOverPresentationController.delegate = self
		//    }
		//
		////    presentViewController(TVC, animated: true, completion: nil)
		//
		//    /*
		//     let VC =  storyboard?.instantiateViewControllerWithIdentifier("AirportTableViewController") as! AirportTableViewController
		//     VC.preferredContentSize = CGSize(width: 300, height: 700)
		//     VC.modalPresentationStyle = .Popover
		//     let AirportPopOverPresentationController = VC.popoverPresentationController
		//
		//     AirportPopOverPresentationController?.delegate = self
		//     //    self.presentViewController(VC, animated: true, completion: nil)
		//
		//     AirportPopOverPresentationController!.sourceView = sender
		//
		//     self.presentViewController(VC, animated: true, completion: nil)
		//     */
	}
	
	
	///leftNavigationBarREFButton
	@IBOutlet weak var leftNavigationBarREF: UIButton!
	
	///leftNavigationBarSTARButton
	@IBOutlet weak var leftNavigationBarSTAR: UIButton!
	
	///leftNavigationBarAPPButton
	@IBOutlet weak var leftNavigationBarAPP: UIButton!
	
	///leftNavigationBarTAXIButton
	@IBOutlet weak var leftNavigationBarTAXI: UIButton!
	
	///leftNavigationBarSIDButton
	@IBOutlet weak var leftNavigationBarSID: UIButton!
	
	
	@IBAction func unwindForSegue(unwindSegue: UIStoryboardSegue) {
		//		refreshInterface(interfaceHandler.DisplayedChartId)
		//let ICAO: String = interfaceHandler.SelectedChartAirport
		//leftNavigationBarICAO.setTitle("\(ICAO)▾", forState: .Normal)
		//loadCharts(ICAO)
	}
	
	func loadCharts(ICAO: String) {
		referenceCharts = chartHelper.GetTypeCharts(ICAO, chartType: ChartTypes.REF.rawValue)
		currentCharts.REF = chartHelper.GetCheckedTypeCharts(ICAO, chartType: ChartTypes.REF.rawValue)
		
		arrivalCharts =	chartHelper.GetTypeCharts(ICAO, chartType: ChartTypes.STAR.rawValue)
		currentCharts.STAR = chartHelper.GetCheckedTypeCharts(ICAO, chartType: ChartTypes.STAR.rawValue)
		
		approachCharts = chartHelper.GetTypeCharts(ICAO, chartType: ChartTypes.APP.rawValue)
		//		approachChartsCountByRunway = chartHelper.GetChartsCountBySections(ICAO, chartType: ChartTypes.APP.rawValue)
		currentCharts.APP = chartHelper.GetCheckedTypeCharts(ICAO, chartType: ChartTypes.APP.rawValue)
		
		taxiCharts = chartHelper.GetTypeCharts(ICAO, chartType: ChartTypes.TAXI.rawValue)
		currentCharts.TAXI = chartHelper.GetCheckedTypeCharts(ICAO, chartType: ChartTypes.TAXI.rawValue)
		
		departureCharts = chartHelper.GetTypeCharts(ICAO, chartType: ChartTypes.SID.rawValue)
		currentCharts.SID = chartHelper.GetCheckedTypeCharts(ICAO, chartType: ChartTypes.SID.rawValue)
		
		currentCharts.load(referenceCharts, star: arrivalCharts, app: approachCharts, taxi: taxiCharts, sid: departureCharts)
		
		setupPageIndicatiors()
		
		GetCurrentAirportChartsCount()
		
		//tableView.reloadData()
	}
	
	// MARK: - FrameStackView
	
 
	
	func GetCurrentAirportChartsCount() {
		currentViewInfo.currentAirportChartsCount[ChartTypes.REF] = chartHelper.GetChartsCountByType(currentViewInfo.currentAirportICAO, ChartType: ChartTypes.REF.rawValue)
		currentViewInfo.currentAirportChartsCount[ChartTypes.STAR] = chartHelper.GetChartsCountByType(currentViewInfo.currentAirportICAO,ChartType: ChartTypes.STAR.rawValue)
		currentViewInfo.currentAirportChartsCount[ChartTypes.APP] = chartHelper.GetChartsCountByType(currentViewInfo.currentAirportICAO,ChartType: ChartTypes.APP.rawValue)
		currentViewInfo.currentAirportChartsCount[ChartTypes.TAXI] = chartHelper.GetChartsCountByType(currentViewInfo.currentAirportICAO,ChartType: ChartTypes.TAXI.rawValue)
		currentViewInfo.currentAirportChartsCount[ChartTypes.SID] = chartHelper.GetChartsCountByType(currentViewInfo.currentAirportICAO,ChartType: ChartTypes.SID.rawValue)
	}
	
	// MARK: - TableView
	/*
	
	
	let chartCellIdentifier = "ChartCell"
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
	switch currentViewInfo.currentChartType {
	//		case ChartTypes.REF.rawValue:
	//			return 1
	//		case ChartTypes.STAR.rawValue:
	//			return 1
	
	case ChartTypes.APP.rawValue:
	//			var sectionsCount: Int!
	//			if let sections = chartHelper.GetSectionedChartsByRunways(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue) {
	//				sectionsCount = sections.count
	//			}
	//			return sectionsCount
	//
	//			return chartHelper.GetSectionedChartsByRunways(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue).count
	return chartHelper.GetChartsSections(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue).count
	
	//			return approachChartHelper.GetApproachChartsSections(currentViewInfo.currentAirportICAO).count
	//		case ChartTypes.TAXI.rawValue:
	//			return 1
	//		case ChartTypes.SID.rawValue:
	//			return 1
	default:
	return 1
	}
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
	switch currentViewInfo.currentChartType {
	case ChartTypes.REF.rawValue:
	return currentViewInfo.currentAirportChartsCount[ChartTypes.REF]!
	case ChartTypes.STAR.rawValue:
	return currentViewInfo.currentAirportChartsCount[ChartTypes.STAR]!
	case ChartTypes.APP.rawValue:
	//			var currentSecitonObjectsCount: Int!
	//			if let sections = chartHelper.GetSectionedChartsByRunways(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue) {
	//				let currentSeciton = sections[section]
	//				currentSecitonObjectsCount = currentSeciton.numberOfObjects
	//			}
	//			return currentSecitonObjectsCount
	//
	var runwaysName:[String]	=		chartHelper.GetChartsRunways(currentViewInfo.currentAirportICAO,chartType: ChartTypes.APP.rawValue)
	var runways: [String : Int] =	chartHelper.GetChartsSections(currentViewInfo.currentAirportICAO,chartType: ChartTypes.APP.rawValue)
	return runways[runwaysName[section]]!
	case ChartTypes.TAXI.rawValue:
	return currentViewInfo.currentAirportChartsCount[ChartTypes.TAXI]!
	case ChartTypes.SID.rawValue:
	return currentViewInfo.currentAirportChartsCount[ChartTypes.SID]!
	
	default:
	return 0
	}
	
	}
	
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
	switch currentViewInfo.currentChartType {
	case ChartTypes.REF.rawValue:
	return "REFERENCE CHARTS \(currentViewInfo.currentAirportChartsCount[ChartTypes.REF]!)"
	case ChartTypes.STAR.rawValue:
	return "ARRIVALS \(currentViewInfo.currentAirportChartsCount[ChartTypes.STAR]!)"
	case ChartTypes.APP.rawValue:
	var runwaysName:[String] = chartHelper.GetChartsRunways(currentViewInfo.currentAirportICAO,chartType: ChartTypes.APP.rawValue)
	return "APPROACH CHARTS \n\(runwaysName[section]) \(chartHelper.GetChartsCountByRunway(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue , Runway: runwaysName[section]))"
	case ChartTypes.TAXI.rawValue:
	return "TAXI CHARTS \(currentViewInfo.currentAirportChartsCount[ChartTypes.TAXI]!)"
	case ChartTypes.SID.rawValue:
	return "DEPARTURES \(currentViewInfo.currentAirportChartsCount[ChartTypes.SID]!)"
	default:
	return "MINE"
	
	}
	
	}
	func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	return true
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
	
	let cell = tableView.dequeueReusableCellWithIdentifier(chartCellIdentifier, forIndexPath: indexPath) as! ChartViewCell
	switch currentViewInfo.currentChartType {
	case ChartTypes.REF.rawValue:
	if currentViewInfo.currentChartType == ChartTypes.REF.rawValue{
	let object = referenceCharts[indexPath.row] as Chart
	configureCell(cell, withObject: object)
	
	return cell
	}
	
	case ChartTypes.STAR.rawValue:
	if currentViewInfo.currentChartType == ChartTypes.STAR.rawValue{
	let object = arrivalCharts[indexPath.row] as NSManagedObject
	configureCell(cell, withObject: object)
	return cell
	}
	case ChartTypes.APP.rawValue:
	if currentViewInfo.currentChartType == ChartTypes.APP.rawValue{
	var runwaysName:[String] = chartHelper.GetChartsRunways(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue)
	let sectionCharts = chartHelper.GetChartsByRunway(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue, Runway: runwaysName[indexPath.section])
	let object = sectionCharts[indexPath.row] as NSManagedObject
	configureCell(cell, withObject: object)
	return cell
	}
	
	/*
	if currentViewInfo.currentChartType == ChartTypes.APP.rawValue{
	//			let chartObject = chartHelper.fetchedResultController.objectAtIndexPath(indexPath) as! NSManagedObject
	//			configureCell(cell, withObject: chartObject)
	//			return cell
	
	var runwaysName:[String] = chartHelper.GetChartsRunways(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue)
	let sectionCharts = chartHelper.GetChartsByRunway(currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue, Runway: runwaysName[indexPath.section])
	let object = sectionCharts[indexPath.row] as NSManagedObject
	configureCell(cell, withObject: object)
	return cell
	}
	*/
	case ChartTypes.TAXI.rawValue:
	if currentViewInfo.currentChartType == ChartTypes.TAXI.rawValue{
	let object = taxiCharts[indexPath.row] as NSManagedObject
	configureCell(cell, withObject: object)
	return cell
	}
	case ChartTypes.SID.rawValue:
	if currentViewInfo.currentChartType == ChartTypes.SID.rawValue{
	let object = departureCharts[indexPath.row] as NSManagedObject
	configureCell(cell, withObject: object)
	return cell
	}
	default:
	break
	}
	return cell
	}
	func configureCell(cell: ChartViewCell, withObject object: NSManagedObject){
	if let entityDescription = object.valueForKey("chartDescription") as? String{
	cell.chartDescriptionLabel.text = entityDescription
	}
	if let entityIndex = object.valueForKey("chartIndex") as? String{
	cell.chartIndexLabel.text = entityIndex
	}
	
	if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
	cell.chartCheckButton.selected = entityCheckStatus
	}
	
	cell.didTapButtonHandler = {
	if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
	
	switch currentViewInfo.currentChartType {
	case ChartTypes.REF.rawValue:
	self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.REF.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
	case ChartTypes.STAR.rawValue:
	self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.STAR.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
	case ChartTypes.APP.rawValue:
	self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
	
	case ChartTypes.TAXI.rawValue:
	self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.TAXI.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
	case ChartTypes.SID.rawValue:
	self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.SID.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
	default:
	break
	}
	
	self.interfaceHandler.chartCheckedUpdate()
	self.interfaceHandler.fillCheckedCharts()
	let chartId: String = object.valueForKey("chartId") as! String
	let checkStatus: Bool =  object.valueForKey("chartChecked") as! Bool
	debugPrint("ID: \(chartId)\(checkStatus)\(entityCheckStatus)")
	//				self.interfaceHandler.handleCurrentDisplayedChart(forChartId: chartId , forChartCheckingStatus: checkStatus)
	
	
	
	self.loadCharts(currentViewInfo.currentAirportICAO)
	}
	}
	}
	
	
	// MARK: -
	// MARK: Fetched Results Controller Delegate Methods
	func controllerWillChangeContent(controller: NSFetchedResultsController) {
	tableView.beginUpdates()
	}
	
	func controllerDidChangeContent(controller: NSFetchedResultsController) {
	tableView.endUpdates()
	}
	
	func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
	print("You WILL selected row: \(indexPath.row) in section: \(indexPath.section)")
	
	print("\(currentViewInfo.currentChartType)")
	
	//		switch currentViewInfo.currentChartType {
	//		case "REF":
	//			currentCharts.CurrentChart = referenceCharts[indexPath.row] as Chart
	//		case "STAR":
	//			currentCharts.CurrentChart = arrivalCharts[indexPath.row] as Chart
	//		case "APP":
	//			var preSectionsRowCount: Int = 0
	//			for i in 0..<indexPath.section{
	//				preSectionsRowCount += approachChartsCountByRunway[i]
	//			}
	//			currentCharts.CurrentChart =  approachCharts[indexPath.row + preSectionsRowCount] as Chart!
	//		case "TAXI":
	//		currentCharts.CurrentChart = taxiCharts[indexPath.row] as Chart
	//		case "SID":
	//		currentCharts.CurrentChart = departureCharts[indexPath.row] as Chart
	//		default:
	//			break
	//		}
	
	
	return indexPath
	
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
	var chartEntity: Chart!
	
	switch currentViewInfo.currentChartType {
	case ChartTypes.REF.rawValue:
	chartEntity = referenceCharts[indexPath.row] as! Chart
	currentViewInfo.currentChartId = chartEntity.chartId!
	//			mainChartImageView.image = UIImage(data: chartEntity.chartImage as NSData)
	case ChartTypes.STAR.rawValue:
	chartEntity = arrivalCharts[indexPath.row] as Chart!
	currentViewInfo.currentChartId = chartEntity.chartId!
	//			mainChartImageView.image = UIImage(data: chartEntity.chartImage as NSData)
	
	case ChartTypes.APP.rawValue:
	var preSectionsRowCount: Int = 0
	for i in 0..<indexPath.section{
	preSectionsRowCount += approachChartsCountByRunway[i]
	}
	
	chartEntity =  approachCharts[indexPath.row + preSectionsRowCount] as Chart!
	currentViewInfo.currentChartId = chartEntity.chartId!
	//			mainChartImageView.image = UIImage(data: chartEntity.chartImage as NSData)
	
	case ChartTypes.TAXI.rawValue:
	chartEntity = taxiCharts[indexPath.row] as Chart!
	currentViewInfo.currentChartId = chartEntity.chartId!
	//			mainChartImageView.image = UIImage(data: chartEntity.chartImage as NSData)
	case ChartTypes.SID.rawValue:
	chartEntity = departureCharts[indexPath.row] as Chart!
	currentViewInfo.currentChartId = chartEntity.chartId!
	//			mainChartImageView.image = UIImage(data: chartEntity.chartImage as NSData)
	default:
	break
	}
	
	
	interfaceHandler.handleCurrentDisplayedChart(forChartId: chartEntity.chartId!, forChartCheckingStatus: chartEntity.chartChecked as! Bool)
	
	//		currentCharts.GetCurrentChartByID(chartEntity.chartId!)
	mainChartImageView.image = UIImage(data: chartEntity.chartImage! as NSData)
	
	// set scroll view min, max and current zoom scale
	//		let currentZoomScale = self.chartsContainerScrollView.bounds.height/self.chartsContainerScrollView.bounds.size.height
	//		self.chartsContainerScrollView.zoomScale = currentZoomScale
	setupPageIndicatiors()
	self.mainNavigation.title	 = "\(interfaceHandler.DisplayedChartId) : \(chartEntity.chartDescription)"
	
	}
	*/
	
}

