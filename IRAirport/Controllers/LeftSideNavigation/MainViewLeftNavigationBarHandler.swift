//
//  MainVieweftNavigationBarHandler.swift
//  IRAirport
//
//  Created by Akbarzade on 5/28/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController  {
	
	
	@IBAction func leftNavigationBarREFButton(sender: UIButton) {
		TerminalChartButtonHandler(ChartType.REF)
	}
	
	//leftNavigationBarSTARButton
	@IBAction func leftNavigationBarSTARButton(sender: UIButton) {
		TerminalChartButtonHandler(ChartType.STAR)
	}
	
	//leftNavigationBarAPPButton
	@IBAction func leftNavigationBarAPPButton(sender: UIButton) {
		TerminalChartButtonHandler(ChartType.APP)
	}
	//leftNavigationBarTAXIButton
	@IBAction func leftNavigationBarTAXIButton(sender: UIButton) {
		TerminalChartButtonHandler(ChartType.TAXI)
	}
	//leftNavigationBarSIDButton
	@IBAction func leftNavigationBarSIDButton(sender: UIButton) {
		TerminalChartButtonHandler(ChartType.SID)
	}
	
	func TerminalChartButtonHandler(selectedChartType: ChartType){
		if Defaults[.isAirportSelected]{
			interfaceHandler.SelectedTerminalChart = selectedChartType
			mainSetTerminalViewDelegate.setTerminalChartTitleLabelText(selectedChartType.fullTypeName())
			mainSetTerminalViewDelegate.setTerminalChart(selectedChartType)
			mainSetTerminalViewDelegate.loadTerminalCharts()
			mainSetTerminalViewDelegate.reloadTerminalCharts()
			printTerminalChartLog()
			
			frameStackViewCharts(selectedChartType)
			
			printTerminalChartLog()
			//		let sections = chartService.GetChartSections(interfaceHandler.SelectedAirport , chartType: chartType.stringValue())
		}
		let circleCenter: CGPoint = setCircleCenter(selectedChartType)
		UIView.animateWithDuration(0.25){
			self.circlePointerLabel.center.y = circleCenter.y
			
		}
		
		func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
			if (segue.identifier == "TerminalChartTableViewSegue" ){
				let terminalChartTVC = segue.destinationViewController as! TerminalChartTableViewController
				
				//terminalChartTVC.terminalChartsDelegate = self
				//							InterfaceManager.initChartArray()
				print("MVC: Segue Detected as : \(segue.identifier!)")
				//terminalChartTVC.tableView.reloadData()
			} else {
				print("Segue not detected.")
			}
			
		}
	}
	
	func setCircleCenter(selectedChartType: ChartType) -> CGPoint {
		var circleCenter: CGPoint
		switch selectedChartType {
		case ChartType.REF:
			circleCenter = self.leftNavigationBarREF.center
		case ChartType.STAR:
			circleCenter = self.leftNavigationBarSTAR.center
		case ChartType.APP:
			circleCenter = self.leftNavigationBarAPP.center
		case ChartType.TAXI:
			circleCenter = self.leftNavigationBarTAXI.center
		case ChartType.SID:
			circleCenter = self.leftNavigationBarSID.center
		}
		return circleCenter
	}
	func frameStackViewCharts(selectedChartType: ChartType){
		let index = 1
		let togglingView = frameStackView.arrangedSubviews[index]
		if interfaceHandler.SelectedTerminalChart == interfaceHandler.DisplayedTerminalChart {
			UIView.animateWithDuration(0.25){togglingView.hidden =  !togglingView.hidden}
			Defaults[.TerminalChartsTogglingStatus] = !(Defaults[.TerminalChartsTogglingStatus])
		}
		else{
			UIView.animateWithDuration(0.25){togglingView.hidden =  false}
			Defaults[.TerminalChartsTogglingStatus] = true
		}
		handleMainChartImageViewSize()
		interfaceHandler.DisplayedTerminalChart = selectedChartType
		//		tableView.reloadData()
	}
	func printTerminalChartLog(){
		let DisplayedChart = interfaceHandler.DisplayedTerminalChart == nil ?  "It's NIL yet..." :
			interfaceHandler.DisplayedTerminalChart.fullTypeName()
		let SelectedChart = interfaceHandler.SelectedTerminalChart == nil ?  "It's NIL yet..." : interfaceHandler.SelectedTerminalChart.fullTypeName()
		let TogglingStatus = Defaults[.TerminalChartsTogglingStatus]  == true ? "Show" : "Hide"
		
		print("Displayed Terminal Chart is: \(DisplayedChart)")
		print("Selected Terminal Chart is: \(SelectedChart)")
		print("Terminal Charts Toggling Status :\(TogglingStatus)")
		print("Circle Pointer Position is : [X:\(self.circlePointerLabel.center.x) , Y:\(self.circlePointerLabel.center.y)]")
	}
}

protocol MainSetTerminalViewDelegate{
	func setTerminalChartTitleLabelText(selectedTerminalChart: String)
	func setTerminalChart(selectedTerminalChart: ChartType)
	func loadTerminalCharts()
	func reloadTerminalCharts()
}
