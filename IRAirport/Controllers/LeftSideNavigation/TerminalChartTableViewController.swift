//
//  TerminalChartTableViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/31/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//
import Foundation
import UIKit


class TerminalChartTableViewController:  UITableViewController , MainSetTerminalViewDelegate {
	
	@IBOutlet weak var TerminalChartTitleLabel: UILabel!
	
	//var terminalChartSelectionDelegate: TerminalChartSelectionDelegate! = nil
	// MARK: - Difinitions
	// MARK: - NOTE
	/// NOTE: - Must be declare and handle
	var ApproachSectionObjects: [TerminalChartAppModel] = []
	var ApproachChartSectionsCount: [Int:Int] = [:]
	var AirportApproachChartRunways: [String] = []
	
	let chartCellIdentifier = "ChartCell"
	
	var ChartTypeREF: [Chart] = []
	var ChartTypeSTAR: [Chart] = []
	var ChartTypeAPP : [Chart] = []
	var ChartTypeTAXI: [Chart] = []
	var ChartTypeSID: [Chart] = []
	var totalCharts: Int = 0
	
	var ChartObjectsArray: [TerminalChartModel] = []
	let airportService = AirportService()
	let chartService = ChartService()
	let interfaceHandler = InterfaceHandler()
	
	func setTerminalChartTitleLabelText(selectedTerminalChart: String) {
		if let terminalChartTitleLabel: String = selectedTerminalChart {
			TerminalChartTitleLabel.text = terminalChartTitleLabel
		} else{
			TerminalChartTitleLabel.text = "Select Airport first"
		}
	}
	

	func setTerminalChart(selectedTerminalChart: ChartType) {
		if let selectedTerminalChartType: ChartType  = selectedTerminalChart {
			interfaceHandler.SelectedTerminalChart = selectedTerminalChartType
		}
		
	}
	
	func loadTerminalCharts() {
		if Defaults[.DisplayedAirport] != Defaults[.DisplayedTerminalChartAirport] || totalCharts == 0{
			initChartArray()
			Defaults[.DisplayedTerminalChartAirport] = Defaults[.DisplayedAirport]
		}
	}
	
	
	func reloadTerminalCharts(){
		
		if Defaults[.TerminalChartsTogglingStatus] {
			tableView.reloadData()
			print("tableView.reloadData() on Toggling")
		}
		
		if interfaceHandler.SelectedTerminalChart != interfaceHandler.DisplayedTerminalChart  {
			tableView.reloadData()
			//interfaceHandler.DisplayedTerminalChart = interfaceHandler.SelectedTerminalChart
			print("tableView.reloadData() on Different Terminal Chart")
		}
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		print("TerminalChartsTableViewController viewDidLoad")
		if Defaults[.isAirportSelected]{
			initChartArray()
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		if interfaceHandler.SelectedTerminalChart == ChartType.APP {
			handleAirportApproachRunways()
			return AirportApproachChartRunways.count
		}
		return 1
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if Defaults[.isAirportSelected] && interfaceHandler.SelectedTerminalChart != nil {
			switch interfaceHandler.SelectedTerminalChart.hashValue {
			case ChartType.REF.hashValue :
				return ChartTypeREF.count
			case ChartType.STAR.hashValue :
				return ChartTypeSTAR.count
			case ChartType.APP.hashValue :
				return ApproachSectionObjects[section].ChartItems.count
			case ChartType.TAXI.hashValue:
				return ChartTypeTAXI.count
			case ChartType.SID.hashValue :
				return ChartTypeSID.count
			default:
				return 0
			}
		}
		return 0
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCellWithIdentifier(chartCellIdentifier, forIndexPath: indexPath) as! ChartViewCell
		var chartItem: Chart!
		//		let chartItem = ChartObjectsArray[interfaceHandler.DisplayedTerminalChart.hashValue].ChartItems[indexPath.row]
		//		print(chartItem.chartId)
		//		configureCell(cell, withObject: chartItem)
		//
		if interfaceHandler.SelectedTerminalChart != nil {
			switch interfaceHandler.SelectedTerminalChart.hashValue {
			case ChartType.REF.hashValue :
				 chartItem = ChartTypeREF[indexPath.row] as Chart
				print("cellForRowAtIndexPath \(chartItem.chartId)")
				configureCell(cell, withObject: chartItem)
				return cell
			case ChartType.STAR.hashValue :
				 chartItem = ChartTypeSTAR[indexPath.row] as Chart
				print("cellForRowAtIndexPath \(chartItem.chartId)")
				configureCell(cell, withObject: chartItem)
				return cell
			case ChartType.APP.hashValue :
				 chartItem = ApproachSectionObjects[indexPath.section].ChartItems[indexPath.row]
				//				let chartItem = ChartObjectsArray[ChartType.APP.hashValue].ChartItems[indexPath.row] as Chart
				print("cellForRowAtIndexPath \(chartItem.chartId)")
				configureCell(cell, withObject: chartItem)
				return cell
			case ChartType.TAXI.hashValue :
				 chartItem = ChartTypeTAXI[indexPath.row] as Chart
				print("cellForRowAtIndexPath \(chartItem.chartId)")
				configureCell(cell, withObject: chartItem)
				return cell
			case ChartType.SID.hashValue :
				 chartItem = ChartTypeSID[indexPath.row] as Chart
				print("cellForRowAtIndexPath \(chartItem.chartId)")
				configureCell(cell, withObject: chartItem)
				return cell
			default:
				break
			}
		}
		
		return cell
	}
	
	override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
		var chartId: String = ""
		
		if interfaceHandler.SelectedTerminalChart != nil {
			switch interfaceHandler.SelectedTerminalChart.hashValue {
			case ChartType.REF.hashValue :
				chartId = ChartTypeREF[indexPath.row].chartId
				print("ChartIdForRowAtIndexPath \(chartId)")
			case ChartType.STAR.hashValue :
				chartId = ChartTypeSTAR[indexPath.row].chartId
				print("ChartIdForRowAtIndexPath \(chartId)")
			case ChartType.APP.hashValue :
				chartId = ApproachSectionObjects[indexPath.section].ChartItems[indexPath.row].chartId
				print("ChartIdForRowAtIndexPath \(chartId)")
			case ChartType.TAXI.hashValue :
				chartId = ChartTypeTAXI[indexPath.row].chartId
			print("ChartIdForRowAtIndexPath \(chartId)")
			case ChartType.SID.hashValue :
				chartId = ChartTypeSID[indexPath.row].chartId
				print("ChartIdForRowAtIndexPath \(chartId)")
			default:
				break
			}
		}
		Defaults[.SelectedChartId] = chartId
		NSNotificationCenter.defaultCenter().postNotificationName(NotificationKey.TerminalChartSelectionNotificationKey, object: self)
		return indexPath
	}
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
	if editingStyle == .Delete {
	// Delete the row from the data source
	tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
	} else if editingStyle == .Insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	@IBAction func unwindForSegue(unwindSegue: UIStoryboardSegue) {
		
		
		
		//let mainVC = storyboard?.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
		
		//mainVC.selectedChartId = Defaults[.SelectedChartId]
//		mainVC.mainChartImageView.image = UIImage(data: selectedChart.chartImage)
	
		//		refreshInterface(interfaceHandler.DisplayedChartId)
		//let ICAO: String = interfaceHandler.SelectedChartAirport
		//leftNavigationBarICAO.setTitle("\(ICAO)▾", forState: .Normal)
		//loadCharts(ICAO)
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	
		if (segue.identifier == "TerminalChartTableViewSegue" ){
			let mainViewController = segue.destinationViewController as! MainViewController
			mainViewController.mainSetTerminalViewDelegate = self

		
			//terminalChartTVC.terminalChartDelegate = self
			//terminalChartTVC.initChartArray()
			print("TCTVC: Segue Detected as : \(segue.identifier!)")
		} else {
			Defaults[.isTerminalChartSelected]  = true
			print("Segue not detected.")
		}
		
	}
	
	func configureCell(cell: ChartViewCell, withObject object: Chart){
		if let entityDescription = object.valueForKey("chartDescription") as? String{
			cell.chartDescriptionLabel.text = entityDescription
		}
		if let entityIndex = object.valueForKey("chartIndex") as? String{
			cell.chartIndexLabel.text = entityIndex
		}
		
		if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
			cell.chartCheckButton.selected = entityCheckStatus
		}
		
		cell.didTapButtonHandler = {
			if let entityCheckStatus = object.valueForKey("chartChecked") as? Bool{
				self.chartService.updateChartCheckStatus(onChartId: object.chartId, setCheckStatusTo: !entityCheckStatus)
				/*
				switch currentViewInfo.currentChartType {
				case ChartTypes.REF.rawValue:
				self.chartHelper.updateChartCheckStatus(object.chartId atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.REF.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.STAR.rawValue:
				self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.STAR.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.APP.rawValue:
				self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.APP.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				
				case ChartTypes.TAXI.rawValue:
				self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.TAXI.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				case ChartTypes.SID.rawValue:
				self.chartHelper.updateChartCheckStatus(atAirport: currentViewInfo.currentAirportICAO, chartType: ChartTypes.SID.rawValue, onChartIndex: object.valueForKey("chartIndex") as! String, setCheckStatusTo: !entityCheckStatus)
				default:
				break
				}
				*/
				
				//self.interfaceHandler.chartCheckedUpdate()
				//self.interfaceHandler.fillCheckedCharts()
				let chartId: String = object.valueForKey("chartId") as! String
				let checkStatus: Bool =  object.valueForKey("chartChecked") as! Bool
				debugPrint("ID: \(chartId)\(checkStatus)\(entityCheckStatus)")
				//				self.interfaceHandler.handleCurrentSelectedChart(forChartId: chartId , forChartCheckingStatus: checkStatus)
				
				self.tableView.reloadData()
				print("tableView.reloadData() on Change Checking")
				// self.loadCharts(currentViewInfo.currentAirportICAO)
			}
		}
	}
	func handleAirportApproachRunways() {
		if Defaults[.isAirportSelected] && interfaceHandler.SelectedTerminalChart == ChartType.APP {
			AirportApproachChartRunways = airportService.GetAirport(Defaults[.SelectedAirport]).airportRunways!.componentsSeparatedByString("|")
		}
		handleApproachSectionObjects()
	}
	func handleApproachSectionObjects(){
		ApproachSectionObjects.removeAll()
		for index in 0..<AirportApproachChartRunways.count {
			let sectionCharts = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.APP.rawValue, sectionName: AirportApproachChartRunways[index]) as [Chart]
			let sectionChartCount = sectionCharts.count
			ApproachSectionObjects.append(TerminalChartAppModel(TerminalChartCount: sectionChartCount, ChartItems: sectionCharts))
		}
	}
	
	func initChartArray(){
		
		ChartTypeREF  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.REF.rawValue)
		ChartTypeSTAR  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.STAR.rawValue)
		ChartTypeAPP  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.APP.rawValue)
		ChartTypeTAXI  = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.TAXI.rawValue)
		ChartTypeSID = chartService.GetTypeCharts(Defaults[.SelectedAirport], chartType: ChartType.SID.rawValue)
		
		totalCharts = ChartTypeREF.count + ChartTypeSTAR.count + ChartTypeAPP.count + ChartTypeTAXI.count + ChartTypeSID.count
		
		
		ChartObjectsArray = [
			TerminalChartModel(TerminalChartType: ChartType.REF.rawValue, ChartItems: ChartTypeREF),
			TerminalChartModel(TerminalChartType: ChartType.STAR.rawValue, ChartItems: ChartTypeSTAR),
			TerminalChartModel(TerminalChartType: ChartType.APP.rawValue, ChartItems: ChartTypeAPP),
			TerminalChartModel(TerminalChartType: ChartType.TAXI.rawValue, ChartItems: ChartTypeTAXI),
			TerminalChartModel(TerminalChartType: ChartType.SID.rawValue, ChartItems: ChartTypeSID)
		]
		
		
		for chart in ChartTypeREF {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeSTAR {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for section in interfaceHandler.AirportApproachChartRunways {
			let sectionItem = section
			print("ChartItem: \(sectionItem)")
		}
		for chart in ChartTypeAPP {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeTAXI {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		for chart in ChartTypeSID {
			let chartItem = chart
			print("ChartItem: \(chartItem.chartId)")
		}
		
		tableView.reloadData()
		print("tableView.reloadData() on initChartArray")
	}
	
	
}
