//
//  AirportsTableViewController.swift
//  IRAirport
//
//  Created by Akbarzade on 5/6/16.
//  Copyright © 2016 Akbarzade. All rights reserved.
//

import UIKit
import CoreData
class AirportsTableViewController: UITableViewController,UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate  {
	
	// MARK: - Definition
	lazy var coreDataStore: CoreDataStore = {
		let coreDataStore = CoreDataStore()
		return coreDataStore
	}()
	
	lazy var coreDataHelper: CoreDataHelper = {
		let coreDataHelper = CoreDataHelper()
		return coreDataHelper
	}()
	let airportService = AirportService()
	let interfaceHandler = InterfaceHandler()
	let interfaceManager = InterfaceManager()
	
	var airportsDelegate: AirportsDelegate! = nil
	var selectedAirportICAO: String! = nil
	
	var fetchedAirports = [Airport]()
	let airportCellIdentifier = "AirportCell"
	
	//
	// MARK: - Loading
	override func viewDidLoad() {
		super.viewDidLoad()
		
		loadAirports()
		
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func loadAirports(){
		print("Begin loading airports")
		fetchedAirports = airportService.GetAirports()
		print("\(fetchedAirports.count) Airports loaded")
	}
	
	//
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		//return fetchedResultsControllerForTableView(tableView)?.sections?.count ?? 0
		return 1
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		//	return fetchedResultsControllerForTableView(tableView)?.sections?[section].numberOfObjects ?? 0
		return fetchedAirports.count
		print("\(fetchedAirports.count) Cell is here")
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		//		let cell = tableView.dequeueReusableCellWithIdentifier(airportCellIdentifier, forIndexPath: indexPath) as! UITableViewCell
		let cell = tableView.dequeueReusableCellWithIdentifier(airportCellIdentifier, forIndexPath: indexPath)
		
		
		// Configure the cell...
		if let airportCell = fetchedAirports[indexPath.row] as? Airport{
			self.configureCell(cell, withAirport: airportCell)
		}
		return cell
	}
	
	
	override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
		let airportICAO =  fetchedAirports[indexPath.row].airportICAO
		
		airportsDelegate.setSelectedAirport(airportICAO)
		airportsDelegate.setAirportButtonText(airportICAO)
		
		
		//	    currentViewInfo.currentAirport = airportICAO
		//	    defaults.setObject(currentViewInfo.currentAirport, forKey: "currentAirport")
		
		//	    let mainViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
		//	    mainViewController.setMainViewCurrentAirport(fetchedAirports[indexPath.row] as AirportModel)
		//	    mainViewController.currentSelectedAirport =  airportICAO
		print("Will be Selecting airport : \(airportICAO)")
		return indexPath
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	
	func configureCell(cell: UITableViewCell, withAirport airport: Airport) {
		var textLabel: String = ""
		var detailTextLabel: String = ""
		textLabel = "\(airport.airportCountry): \(airport.airportICAO)"
		detailTextLabel = "\(airport.airportName)\n in \(airport.airportCity)"
		cell.textLabel!.text = textLabel
		cell.detailTextLabel!.text = detailTextLabel
	}
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		/*
		if let indexPath = self.tableView.indexPathForSelectedRow {
		let currentAirportObject: Airport = self.fetchedAirports[indexPath.row]
		
		defaults.setObject(currentAirportObject.airportICAO, forKey: "SelectedAirportICAO")
		interfaceHandler.SelectedChartAirport = currentAirportObject.airportICAO
		interfaceHandler.chartCheckedUpdate()
		}
		
		*/
		//			currentViewInfo.currentAirportICAO = currentAirportObject.airportICAO
		//			      defaults.setObject(currentViewInfo.currentAirportICAO, forKey: "currentAirportICAO")
		//			      controller.detailItem = object
		//			      controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
		//			      controller.navigationItem.leftItemsSupplementBackButton = true
	}
}

// MARK: - Protocol
protocol AirportsDelegate {
	func setSelectedAirport(ICAO: String)
	func setAirportButtonText(ICAO: String)
}