////
////  ChartsPageViewController.swift
////  IRAirport
////
////  Created by Akbarzade on 5/10/16.
////  Copyright © 2016 Akbarzade. All rights reserved.
////
//
//import UIKit
//import CoreData
//
//class ChartsPageViewController: UIPageViewController{
//	
//	weak var chartsDelegate: ChartsPageViewControllerDelegate?
//	
//	private(set) lazy var orderedCharts: [UIViewController] = {
//		// The view controllers will be shown in this order
//		return []
//	}()
//	
//	override func viewDidLoad() {
//		super.viewDidLoad()
//		
//		dataSource = self
//		delegate = self
//		
//		if let initialChart = orderedCharts.first {
//			scrollToChart(initialChart)
//		}
//		
//		chartsDelegate?.chartsPageViewController(self,
//		                                     didUpdatePageCount: orderedCharts.count)
//	}
//	/*
//	override func didReceiveMemoryWarning() {
//		super.didReceiveMemoryWarning()
//		// Dispose of any resources that can be recreated.
//	}
//	*/
//	
//	
//	/**
//	Scrolls to the next view controller.
//	*/
//	
////	func scrollToNextChart() {
//		func scrollToNextChartContent(){
//		if let visibleChart = viewControllers?.first,
//			let nextChart = chartsPageViewController(
//				self, viewControllerAfterViewController: visibleChart) {
//			scrollToChart(nextChart)
//		}
//	}
//	
//	/**
//	Scrolls to the view controller at the given index. Automatically calculates
//	the direction.
//	
//	- parameter newIndex: the new index to scroll to
//	*/
//	func scrollToChart(index newIndex: Int) {
//		if let firstChart = viewControllers?.first,
//			let currentIndex = orderedCharts.indexOf(firstChart) {
//			let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .Forward : .Reverse
//			let nextChart = orderedCharts[newIndex]
//			scrollToChart(nextChart, direction: direction)
//		}
//	}
//	
//	/*
//	private func newColoredViewController(color: String) -> UIViewController {
//		return UIStoryboard(name: "Main", bundle: nil) .
//			instantiateViewControllerWithIdentifier("\(color)ViewController")
//	}
//	*/
//	
//	/**
//	Scrolls to the given 'viewController' page.
//	
//	- parameter viewController: the view controller to show.
//	*/
//	private func scrollToChart(viewController: UIViewController,
//	                                    direction: UIPageViewControllerNavigationDirection = .Forward) {
//		setViewControllers([viewController],
//		                   direction: direction,
//		                   animated: true,
//		                   completion: { (finished) -> Void in
//												// Setting the view controller programmatically does not fire
//												// any delegate methods, so we have to manually notify the
//												// 'tutorialDelegate' of the new index.
//												self.notifyChartDelegateOfNewIndex()
//		})
//	}
//	
//	/**
//	Notifies '_tutorialDelegate' that the current page index was updated.
//	*/
//	private func notifyChartDelegateOfNewIndex() {
//		if let firstChart = viewControllers?.first,
//			let index = orderedCharts.indexOf(firstChart) {
//			chartsDelegate?.chartsPageViewController(self,
//			                                     didUpdatePageIndex: index)
//		}
//	}
//	
//}
//
//// MARK: UIPageViewControllerDataSource
//extension ChartsPageViewController: UIPageViewControllerDataSource {
//	
//	func pageViewController(pageViewController: UIPageViewController,
//	                        viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
//		guard let viewControllerIndex = orderedCharts.indexOf(viewController) else {
//			return nil
//		}
//		
//		let previousIndex = viewControllerIndex - 1
//		
//		// User is on the first view controller and swiped left to loop to
//		// the last view controller.
//		guard previousIndex >= 0 else {
//			return orderedCharts.last
//		}
//		
//		guard orderedCharts.count > previousIndex else {
//			return nil
//		}
//		
//		return orderedCharts[previousIndex]
//	}
//	
//	func pageViewController(pageViewController: UIPageViewController,
//	                        viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
//		guard let viewControllerIndex = orderedCharts.indexOf(viewController) else {
//			return nil
//		}
//		
//		let nextIndex = viewControllerIndex + 1
//		let orderedChartsCount = orderedCharts.count
//		
//		// User is on the last view controller and swiped right to loop to
//		// the first view controller.
//		guard orderedChartsCount != nextIndex else {
//			return orderedCharts.first
//		}
//		
//		guard orderedChartsCount > nextIndex else {
//			return nil
//		}
//		
//		return orderedCharts[nextIndex]
//	}
//	
//}
//// MARK: UIPageViewControllerDelegate
//extension ChartsPageViewController: UIPageViewControllerDelegate {
//	
//	func pageViewController(pageViewController: UIPageViewController,
//	                        didFinishAnimating finished: Bool,
//	                                           previousViewControllers: [UIViewController],
//	                                           transitionCompleted completed: Bool) {
//		notifyChartDelegateOfNewIndex()
//	}
//	
//}
//
//protocol ChartsPageViewControllerDelegate: class {
//	
//	/**
//	Called when the number of pages is updated.
//	
//	- parameter chartsPageViewController: the chartsPageViewController instance
//	- parameter count: the total number of pages.
//	*/
//	func chartsPageViewController(chartsPageViewController: PageViewController,
//	                                didUpdatePageCount count: Int)
//	
//	/**
//	Called when the current index is updated.
//	
//	- parameter chartsPageViewController: the chartsPageViewController instance
//	- parameter index: the index of the currently visible page.
//	*/
//	func chartsPageViewController(chartsPageViewController: PageViewController,
//	                                didUpdatePageIndex index: Int)
//	
//}
//
