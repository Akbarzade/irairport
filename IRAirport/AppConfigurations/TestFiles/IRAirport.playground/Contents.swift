//: Playground - noun: a place where people can play

import UIKit

enum ChartTypes  : String {
	case REF  = "REF"
	case STAR  = "STAR"
	case APP   = "APP"
	case TAXI  = "TAXI"
	case SID   = "SID"
	case MINE  = "MINE"
	
	func toString() -> String {
		return String(self)
	}
}

ChartTypes.self

ChartTypes.REF
ChartTypes.REF.hashValue
ChartTypes.REF.rawValue
ChartTypes.REF.toString()

ChartTypes.STAR
ChartTypes.STAR.hashValue
ChartTypes.STAR.rawValue

ChartTypes.APP
ChartTypes.APP.hashValue
ChartTypes.APP.rawValue
